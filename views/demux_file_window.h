/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEMUX_FILE_H
#define DEMUX_FILE_H

#include <QWidget>
#include <QDir>
#include <QStatusBar>
#include <QFileDialog>
#include <QKeyEvent>
#include "classes/dvb_class.h"
#include "threads/dvbtune_thread.h"

namespace Ui {
class demux_file_window;
}

class demux_file_window : public QWidget
{
	Q_OBJECT

public:
	explicit demux_file_window(dvb_class *d);
	~demux_file_window();

	dvb_class *dvb;

	void init();
	dvbtune_thread *mydvbtune_thread;
	QString filename;

public slots:
	void on_pushButton_stop_clicked();

private slots:
	void on_pushButton_start_clicked();
	void on_pushButton_browse_clicked();
	void demux_status(int bytes);

private:
	Ui::demux_file_window *ui;
	QStatusBar *mystatus;
	unsigned long int bytes_wrote;

protected:
	void closeEvent(QCloseEvent *event);
	void keyPressEvent(QKeyEvent *event);
};

#endif // DEMUX_FILE_H
