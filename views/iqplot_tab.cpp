/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "views/iqplot_tab.h"
#include "ui_iqplot_tab.h"

iqplot_tab::iqplot_tab(QWidget *parent, dvb_class *d) :
	QWidget(parent),
	ui(new Ui::iqplot_tab)
{
	ui->setupUi(this);

	dvb = d;

	int gr = 255/MAX_GRADIANT;
	for (unsigned int i = 0; i < MAX_GRADIANT; i++) {
		scatter_symbol[i] = new QwtSymbol;
		scatter_symbol[i]->setStyle(QwtSymbol::Rect);
		scatter_symbol[i]->setSize(1,1);
		scatter_symbol[i]->setPen(QColor(0, gr*i + gr, 0));
		scatter_symbol[i]->setBrush(QColor(0, gr*i + gr, 0));
		curve[i] = new QwtPlotCurve("Curve");
		curve[i]->setStyle(QwtPlotCurve::NoCurve);
		curve[i]->attach(ui->qwtPlot);
		curve[i]->setSymbol(scatter_symbol[i]);
	}

	ui->qwtPlot->setLineWidth(4);
	ui->qwtPlot->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	ui->qwtPlot->setAxisScale(QwtPlot::xBottom, -128, 128);
	ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -128, 128);
	ui->qwtPlot->enableAxis(QwtPlot::xBottom ,0);
	ui->qwtPlot->enableAxis(QwtPlot::yLeft ,0);
	ui->qwtPlot->setCanvasBackground(Qt::darkGray);

	scaleX = new QwtPlotScaleItem();
	scaleX->setAlignment(QwtScaleDraw::BottomScale);
	scale_eng = new QwtLinearScaleEngine();
	scaleX->setScaleDiv(scale_eng->divideScale(-128, 128, 10, 5));
	scaleX->attach(ui->qwtPlot);
	scaleY = new QwtPlotScaleItem();
	scaleY->setAlignment(QwtScaleDraw::LeftScale);
	scaleY->setScaleDiv(scale_eng->divideScale(-128, 128, 10, 5));
	scaleY->attach(ui->qwtPlot);

	connect(dvb->dvb_thread, &dvbtune_thread::iqdraw, this, &iqplot_tab::iqdraw);

	if (dvb->dvb_thread->caps & FE_CAN_8VSB) {
		ui->comboBox_mode->setEnabled(false);
		ui->comboBox_point->setEnabled(false);
	} else {
		ui->comboBox_mode->setEnabled(true);
		ui->comboBox_point->setEnabled(true);
	}

	modcod_name.append("QPSK 1/4");
	modcod_name.append("QPSK 1/3");
	modcod_name.append("QPSK 2/5");
	modcod_name.append("QPSK 1/2");
	modcod_name.append("QPSK 3/5");
	modcod_name.append("QPSK 2/3");
	modcod_name.append("QPSK 3/4");
	modcod_name.append("QPSK 4/5");
	modcod_name.append("QPSK 5/6");
	modcod_name.append("QPSK 8/9");
	modcod_name.append("QPSK 9/10");
	modcod_name.append("8PSK 3/5");
	modcod_name.append("8PSK 2/3");
	modcod_name.append("8PSK 3/4");
	modcod_name.append("8PSK 5/6");
	modcod_name.append("8PSK 8/9");
	modcod_name.append("8PSK 9/10");
	modcod_name.append("16PSK 2/3");
	modcod_name.append("16PSK 3/4");
	modcod_name.append("16PSK 4/5");
	modcod_name.append("16PSK 5/6");
	modcod_name.append("16PSK 8/9");
	modcod_name.append("16PSK 9/10");
	modcod_name.append("32PSK 3/4");
	modcod_name.append("32PSK 4/5");
	modcod_name.append("32PSK 5/6");
	modcod_name.append("32PSK 8/9");
	modcod_name.append("32PSK 9/10");

	int x = -119;
	for(QString t : modcod_name) {
		modcod_marker.append(new QwtPlotMarker);
		modcod_marker.last()->setLabel(t);
		modcod_marker.last()->setLabelAlignment(Qt::AlignCenter|Qt::AlignBottom);
		modcod_marker.last()->setLabelOrientation(Qt::Vertical);
		modcod_marker.last()->setLineStyle(QwtPlotMarker::VLine);
		modcod_marker.last()->setLinePen(Qt::blue,0,Qt::DotLine);
		modcod_marker.last()->setValue(x,0);
		x += 8;
	}
	dvb->dvb_thread->start();
	on_pushButton_onoff_clicked();
}

iqplot_tab::~iqplot_tab()
{
	if (dvb->dvb_thread->thread_function.contains("iqplot")) {
		dvb->dvb_thread->thread_function.remove(dvb->dvb_thread->thread_function.indexOf("iqplot"));
	}

	ui->qwtPlot->detachItems();

	delete scale_eng;
	delete ui->qwtPlot;
	delete ui;
}

void iqplot_tab::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event);

	// You have to give it some index
	emit tab_closed(-1);
}

void iqplot_tab::keyPressEvent(QKeyEvent *event)
{
	if (event->modifiers() == Qt::CTRL && event->key() == Qt::Key_Tab) {
		emit ctrl_tab();
	}
	if (event->key() == Qt::Key_Escape) {
		emit tab_closed(-1);
	}
}

void iqplot_tab::iqdraw(QVector<short int> x, QVector<short int> y)
{
	QVector<double> xs[MAX_GRADIANT];
	QVector<double> ys[MAX_GRADIANT];
	bool xys[MAX_GRADIANT][0xFFFF];

	int scale = 0;

	for (unsigned short int a = 0; a < MAX_GRADIANT; a++) {
		memset(xys[a], false, 0xFFFF);
	}
	for (unsigned short int i = 0; i < x.size(); i++) {
		// x[i] 0xF0 + y[i] 0x0F makes a unique hash, >>1 is simply /2 to make the hash match anything in a 2x2 pixel block vs a 1 for 1 pixel block
		unsigned short int xy_tmp = ((unsigned char)x[i]<<8) + (unsigned char)y[i];
		for (unsigned short int a = 0; a < MAX_GRADIANT; a++) {
			if (!xys[a][xy_tmp]) {
				xys[a][xy_tmp] = true;
				xs[a].append(x[i]);
				ys[a].append(y[i]);
				if (abs(x[i]) > scale) {
					scale = abs(x[i]);
				}
				if (abs(y[i]) > scale) {
					scale = abs(y[i]);
				}
				goto next;
			}
		}
next:
		continue;
	}
	for (unsigned short int a = 0; a < MAX_GRADIANT; a++) {
		curve[a]->setSamples(xs[a], ys[a]);
	}
	scaleX->setScaleDiv(scale_eng->divideScale(scale * -1, scale, 10, 5));
	scaleY->setScaleDiv(scale_eng->divideScale(scale * -1, scale, 10, 5));
	ui->qwtPlot->setAxisScale(QwtPlot::xBottom, scale * -1, scale);
	ui->qwtPlot->setAxisScale(QwtPlot::yLeft, scale * -1, scale);
	ui->qwtPlot->replot();

	if (ui->qwtPlot->size().height() != ui->qwtPlot->size().width()) { // Make it square
		ui->qwtPlot->resize(ui->qwtPlot->size().height(), ui->qwtPlot->size().height());
		ui->gridLayout->setColumnMinimumWidth(1, ui->qwtPlot->size().height());
	}
}

void iqplot_tab::on_pushButton_onoff_clicked()
{
	if (dvb->dvb_thread->thread_function.contains("iqplot")) {
		dvb->dvb_thread->thread_function.remove(dvb->dvb_thread->thread_function.indexOf("iqplot"));
		ui->pushButton_onoff->setText("Start");
	} else {
		erase();
		dvb->dvb_thread->thread_function.append("iqplot");
		ui->pushButton_onoff->setText("Stop");
	}
}

void iqplot_tab::on_comboBox_mode_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	erase();
}

void iqplot_tab::on_comboBox_point_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	erase();
}

void iqplot_tab::erase()
{
	dvb->dvb_thread->iq_options = ui->comboBox_mode->currentIndex() << 4 | ui->comboBox_point->currentIndex();

	sleep(1);

	dvb->dvb_thread->iq_x.clear();
	dvb->dvb_thread->iq_y.clear();

	if (ui->comboBox_point->currentIndex() == 14)
	{
		scaleY->detach();
		for (QwtPlotMarker *t : modcod_marker) {
			t->attach(ui->qwtPlot);
		}
	} else {
		scaleY->attach(ui->qwtPlot);
		for (QwtPlotMarker *t : modcod_marker) {
			t->detach();
		}
	}
}
