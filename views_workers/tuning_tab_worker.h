/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUNING_TAB_WORKER_H
#define TUNING_TAB_WORKER_H

#include <QtCore>
#include <QObject>
#include "classes/dvb_class.h"
#include "threads/tuning_tab_thread.h"
#include "classes/dvbstream_class.h"
#include "views/iqplot_tab.h"
#include "views/demux_dvr_dialog.h"
#include "views/demux_file_window.h"
#include "views_workers/tuning_tab_worker.h"

class tuning_tab_worker : public QObject
{
	Q_OBJECT
signals:
	void server_new();
	void server_close();
	void server_disconnect();

public slots:
	void parsetp_done();
	void parsetp_start();

public:
	tuning_tab_worker(dvb_class *d);
	~tuning_tab_worker();

	dvb_class *dvb;

	void demux_play();
	void demux_save(QString filename);
	void demux_dvr();
	void demux_http();
	void demux_https_disconnect();
	void setup_demux(QVector<unsigned int> pids);

	QPointer<demux_file_window> mydemux_file_window;

	QProcess myProcess;

	bool parsetp_started;

	tuning_tab_thread *mythread;
	dvbstream_class *mystream;
	QThread mystream_thread;
	QThread reader_thread;

	bool isRunning;
};

#endif // TUNING_TAB_WORKER_H
