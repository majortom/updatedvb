/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "views/main_window.h"
#include "ui_main_window.h"

main_window::main_window(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::main_window)
{
	shutdown = false;
	ui->setupUi(this);
	qRegisterMetaType<double>("double");
	qRegisterMetaType<QVector<double> >("QVector<double>");
	qRegisterMetaType<short int>("short int");
	qRegisterMetaType<QVector<short int> >("QVector<short int>");
	this->setFocusPolicy(Qt::StrongFocus);

	status_timer = new QTimer;
	connect(&status_mapper, SIGNAL(mapped(QString)), this, SLOT(remove_status(QString)));

	mc = new master_class;

	if (mc->mysettings->value("main_window_width").toInt()) {
		this->resize(mc->mysettings->value("main_window_width").toInt(), mc->mysettings->value("main_window_height").toInt());
	}

	for (int t : get_adapters()) {
		dvb_class *d = new dvb_class(mc);
		d->dvb_thread->adapter = t;
		connect(d, &dvb_class::update_status, this, &main_window::update_status);

		myadapter_tab.append(new adapter_tab(this, d));
		ui->tabWidget->addTab(myadapter_tab.last(), "");
	}
	name_tabs();

	// Check if any adapters are found
	if (!ui->tabWidget->count()) {
		QMessageBox welcome;
		welcome.setText("No Adapters Found\nCheck that your adapters are installed correctly and that the kernel modules are installed and loaded");
		welcome.exec();

		close();
		exit(0);
	}

	// First time running, lets make the user edit his settings
	if (!mc->mysettings->value("adapter0_name").isValid()) {
		on_actionSettings_triggered();
	}
}

main_window::~main_window()
{
	delete ui;
}

void main_window::on_actionSettings_triggered()
{
	QVector<dvb_class *> d;
	for (adapter_tab *t : myadapter_tab) {
		d.append(t->dvb);
	}
	settings_window settings_dialog(this, d);
	settings_dialog.startup();
	settings_dialog.setModal(true);
	settings_dialog.exec();

	for (adapter_tab *t : myadapter_tab) {
		t->reload_lnb();
	}
	name_tabs();
}

void main_window::on_actionExit_triggered()
{
	close();
}

void main_window::on_actionSave_Screenshot_triggered()
{
	QString filename;

	filename = mc->mysettings->value("savelocation").toString() + "/rfscan_adapter" + QString::number(myadapter_tab.at(ui->tabWidget->currentIndex())->dvb->dvb_thread->adapter) + ".png";
	filename = QFileDialog::getSaveFileName(this, "Save Screen Shot", filename, tr("Image Files (*.png)"));

	if (filename.isEmpty()) {
		return;
	}

	QPixmap p = myadapter_tab.at(ui->tabWidget->currentIndex())->grab_qwt_plot();
	p.save(filename, "PNG");
}

void main_window::on_tabWidget_currentChanged(int index)
{
	if (shutdown)
		return;
	myadapter_tab.at(index)->myspectrumscan_tab->reload_qwtplot();
}

void main_window::name_tabs()
{
	for (int i = 0; i < myadapter_tab.size(); i++) {
		if (mc->mysettings->value("adapter" + QString::number(myadapter_tab.at(i)->dvb->dvb_thread->adapter) + "_name").toString() !="") {
			ui->tabWidget->setTabText(i, QString::number(myadapter_tab.at(i)->dvb->dvb_thread->adapter) + ":" + mc->mysettings->value("adapter" + QString::number(myadapter_tab.at(i)->dvb->dvb_thread->adapter) + "_name").toString());
		} else {
			ui->tabWidget->setTabText(i, "Adapter " + QString::number(myadapter_tab.at(i)->dvb->dvb_thread->adapter));
		}
	}
}

QVector<int> main_window::get_adapters()
{
	QVector<int> adaps;
	QDir dvb_dir("/dev/dvb");
	dvb_dir.setFilter(QDir::Dirs|QDir::NoDotAndDotDot);
	QStringList adapter_entries = dvb_dir.entryList();
	adapter_entries = adapter_entries.filter("adapter");
	for (QString a : adapter_entries) {
		a.replace("adapter", "");
		adaps.append(a.toInt());
	}
	qSort(adaps);
	return adaps;
}

void main_window::status_set_timer(QString text, int time)
{
	status_timer->setSingleShot(true);
	connect(status_timer, SIGNAL(timeout()), &status_mapper, SLOT(map()));
	status_mapper.setMapping(status_timer, text);
	status_timer->start(time*1000);
}

void main_window::remove_status(QString text)
{
	update_status(text, STATUS_REMOVE);
}

void main_window::update_status(QString text, int time)
{
	if (time == STATUS_CLEAR) {
		mystatus.clear();
	}
	if (time == STATUS_REMOVE) {
		if (mystatus.contains(text)) {
			mystatus.remove(mystatus.indexOf(text));
		}
	}
	if (time == STATUS_NOEXP) {
		mystatus.append(text);
	}
	if (time > 0) {
		status_set_timer(text, time);
		mystatus.append(text);
	}

	if (shutdown)
		return;

	if (mystatus.size()) {
		ui->statusBar->showMessage(mystatus.last(), 0);
	} else {
		ui->statusBar->showMessage("", 0);
	}
}

void main_window::closeEvent(QCloseEvent* ce)
{
	Q_UNUSED(ce);

	shutdown = true;

	for (adapter_tab *t : myadapter_tab) {
		t->deleteLater();
	}

	mc->mysettings->setValue("main_window_width", this->width());
	mc->mysettings->setValue("main_window_height", this->height());
}
