/*	
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "blindscan_save_dialog.h"
#include "ui_blindscan_save_dialog.h"

blindscan_save_dialog::blindscan_save_dialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::blindscan_save_dialog)
{
	ui->setupUi(this);
	ui->lineEdit_filename->setText(QDir::currentPath() + "/dvb-scan.conf");

	mystatus = new QStatusBar;
	ui->verticalLayout->addWidget(mystatus);
}

blindscan_save_dialog::~blindscan_save_dialog()
{
	delete mystatus;
	delete ui;
}

void blindscan_save_dialog::on_pushButton_save_clicked()
{
	QFile *out_fd = new QFile(ui->lineEdit_filename->text());
	if (!out_fd->open(QIODevice::WriteOnly | QIODevice::Text)) {
		return;
	}
	QTextStream out(out_fd);

	for(tp_info t : mytp_info) {
		if (t.system == SYS_DVBS2) {
			out << "S2 ";
		} else {
			out << "S ";
		}
		out << QString::number(t.frequency*1000);
		out << dvbnames.voltage[t.polarity];
		out << QString::number(t.symbolrate*1000) << " ";
		out << dvbnames.fec(t.fec) << " ";
		out << dvbnames.rolloff(t.rolloff) << " ";
		out << dvbnames.modulation(t.modulation) << "\n";
	}
	mystatus->showMessage("Saved");
	emit QDialog::accept();
}

void blindscan_save_dialog::on_pushButton_cancel_clicked()
{
	emit QDialog::accept();
}
