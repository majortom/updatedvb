/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DVB_SETTINGS_H
#define DVB_SETTINGS_H

#include <QString>
#include <QVector>

class dvb_settings
{
public:
	dvb_settings();
	QVector<QString> fec_name;
	QString fec(int val);
	QVector<QString> system_name;
	QString system(int val);
	QVector<QString> modulation_name;
	QString modulation(int val);
	QVector<QString> dtag_modulation;
	QVector<QString> dtag_rolloff;
	QVector<QString> dtag_fec;
	QVector<QString> dtag_polarization;
	QVector<QString> rolloff_name;
	QString rolloff(int val);
	QVector<QString> pilot_name;
	QString pilot(int val);
	QVector<QString> inversion_name;
	QString inversion(int val);
	QVector<QString> tone;
	QVector<QString> voltage;
	QVector<QString> stream_type;
	QVector<QString> table_name;
	QVector<QString> ca_name;
	QVector<QString> dvb_descriptortag;
};

#endif // DVB_SETTINGS_H
