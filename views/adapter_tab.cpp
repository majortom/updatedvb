/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "adapter_tab.h"
#include "ui_adapter_tab.h"

adapter_tab::adapter_tab(QWidget *parent, dvb_class *d) :
	QWidget(parent),
	ui(new Ui::adapter_tab)
{
	ui->setupUi(this);

	dvb = d;

	myspectrumscan_tab = new spectrumscan_tab(this, dvb);
	ui->tabWidget->addTab(myspectrumscan_tab, "Spectrum Scan");
	connect(myspectrumscan_tab, &spectrumscan_tab::new_tuning_tab, this, &adapter_tab::new_tuning_tab);
	connect(myspectrumscan_tab, &spectrumscan_tab::new_blinds_tab, this, &adapter_tab::new_blinds_tab);
	connect(myspectrumscan_tab, &spectrumscan_tab::ctrl_tab, this, &adapter_tab::ctrl_tab);

	ui->tabWidget->setTabsClosable(true);
	connect(ui->tabWidget, &QTabWidget::tabCloseRequested, this, &adapter_tab::tab_closed);

	connect(dvb->dvb_thread, &dvbtune_thread::update_signal, this, &adapter_tab::update_signal);
}

adapter_tab::~adapter_tab()
{
	delete ui;
}

void adapter_tab::tab_closed(int index)
{
	// Dont close the spectrumscan tab
	if (index == 0) {
		return;
	}

	ui->tabWidget->setCurrentIndex(index);
	ui->tabWidget->currentWidget()->deleteLater();
}

void adapter_tab::ctrl_tab()
{
	int i = ui->tabWidget->currentIndex();
	i++;
	if (i == (ui->tabWidget->count())) {
		i = 0;
	}
	ui->tabWidget->setCurrentIndex(i);
}

void adapter_tab::new_tuning_tab()
{
	if (!mytuning_tab.isNull()) {
		dvb->emit_update_status("Tuning tab already open...", 3);
		return;
	}
	mytuning_tab = new tuning_tab(this, dvb);
	int i = ui->tabWidget->addTab(mytuning_tab, "Tuning");
	ui->tabWidget->setCurrentIndex(i);
	connect(mytuning_tab, &tuning_tab::tab_closed, this, &adapter_tab::tab_closed);
	connect(mytuning_tab, &tuning_tab::destroyed, myspectrumscan_tab, &spectrumscan_tab::hide_signal);
	connect(mytuning_tab, &tuning_tab::ctrl_tab, this, &adapter_tab::ctrl_tab);
	connect(mytuning_tab, &tuning_tab::new_iqplot_tab, this, &adapter_tab::new_iqplot_tab);
}

void adapter_tab::new_iqplot_tab()
{
	if (!myiqplot_tab.isNull()) {
		dvb->emit_update_status("IQ Plot tab already open...", 3);
		return;
	}
	myiqplot_tab = new iqplot_tab(this, dvb);
	int i = ui->tabWidget->addTab(myiqplot_tab, "IQ Plot");
	ui->tabWidget->setCurrentIndex(i);
	connect(myiqplot_tab, &iqplot_tab::tab_closed, this, &adapter_tab::tab_closed);
	connect(myiqplot_tab, &iqplot_tab::ctrl_tab, this, &adapter_tab::ctrl_tab);
}

void adapter_tab::new_blinds_tab(QString s)
{
	if (!myblindscan_tab.isNull()) {
		dvb->emit_update_status("Blindscan tab already open...", 3);
		return;
	}
	myblindscan_tab = new blindscan_tab(this, dvb);
	int i = ui->tabWidget->addTab(myblindscan_tab, "Blindscan");
	ui->tabWidget->setCurrentIndex(i);
	connect(myblindscan_tab, &blindscan_tab::tab_closed, this, &adapter_tab::tab_closed);
	connect(myblindscan_tab, &blindscan_tab::ctrl_tab, this, &adapter_tab::ctrl_tab);
	connect(myblindscan_tab, &blindscan_tab::new_tuning_tab, this, &adapter_tab::new_tuning_tab);

	myblindscan_tab->worker->setup_freq();

	if (s == "smartscan") {
		myblindscan_tab->smartscan();
		return;
	}
	if (s == "scan") {
		myblindscan_tab->scan();
		return;
	}
}

void adapter_tab::reload_lnb()
{
	myspectrumscan_tab->reload_lnb();
}

QPixmap adapter_tab::grab_qwt_plot()
{
	return myspectrumscan_tab->grab_qwt_plot();
}

void adapter_tab::update_signal()
{
	if (!mytuning_tab.isNull()) {
		mytuning_tab->update_signal();
	}
	myspectrumscan_tab->update_signal();
}
