/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tuning_tab_worker.h"

tuning_tab_worker::tuning_tab_worker(dvb_class *d)
{
	dvb = d;

	mythread = new tuning_tab_thread(dvb);

	mystream = new dvbstream_class(dvb);
	mystream->moveToThread(&mystream_thread);
	mystream_thread.start();

	parsetp_started = false;

	connect(&myProcess, SIGNAL(finished(int)), this, SLOT(parsetp_start()));
	connect(dvb->dvb_thread->mydvr, &dvr_thread::data, mystream, &dvbstream_class::stream);
	connect(this, &tuning_tab_worker::server_close, mystream, &dvbstream_class::server_close);
	connect(this, &tuning_tab_worker::server_disconnect, mystream, &dvbstream_class::server_disconnect);
	connect(this, &tuning_tab_worker::server_new, mystream, &dvbstream_class::server_new);

	connect(mythread, &tuning_tab_thread::parsetp_done, this, &tuning_tab_worker::parsetp_done);

	dvb->dvb_thread->start();
	dvb->dvb_thread->thread_function.append("tune");
}

tuning_tab_worker::~tuning_tab_worker()
{
	emit server_close();
	while (mystream_thread.isRunning()) {
		mystream_thread.quit();
		mystream_thread.wait(100);
	}

	while (mythread->isRunning()) {
		dvb->dvb_thread->demux_packets_loop = false;
		mythread->loop = false;
		mythread->quit();
		mythread->wait(100);
	}
	mythread->deleteLater();
}

void tuning_tab_worker::setup_demux(QVector<unsigned int> pids)
{
	if (myProcess.pid()) {
		qDebug() << "stopping previous player first...";
		myProcess.kill();
		myProcess.waitForFinished();
	}
	while (mythread->isRunning()) {
		dvb->dvb_thread->demux_packets_loop = false;
		mythread->loop = false;
		mythread->quit();
		mythread->wait(100);
	}
	dvb->dvb_thread->close_dvr();
	dvb->dvb_thread->pids = pids;
	dvb->dvb_thread->demux_video();
}

void tuning_tab_worker::demux_play()
{
	QString cmd = dvb->mc->mysettings->value("cmd_play").toString();
	cmd.replace("{}", QString::number(dvb->dvb_thread->adapter));
	myProcess.start(cmd);
}

void tuning_tab_worker::demux_save(QString filename)
{
	mydemux_file_window = new demux_file_window(dvb);
	connect(mydemux_file_window, &demux_file_window::destroyed, this, &tuning_tab_worker::parsetp_start);

	mydemux_file_window->filename = dvb->mc->mysettings->value("savelocation").toString() + "/" + filename;
	mydemux_file_window->init();
	mydemux_file_window->show();
}

void tuning_tab_worker::demux_dvr()
{
	static bool is_running = false;
	if (is_running) {
		parsetp_start();
		dvb->emit_update_status("Sending data to " + dvb->dvb_thread->dvr_name, STATUS_REMOVE);
		is_running = false;
	} else {
		dvb->dvb_thread->close_dvr();
		dvb->emit_update_status("Sending data to " + dvb->dvb_thread->dvr_name, STATUS_NOEXP);
		is_running = true;
	}
}

void tuning_tab_worker::demux_http()
{
	emit server_new();
}

void tuning_tab_worker::demux_https_disconnect()
{
	emit server_disconnect();
	parsetp_start();
}

void tuning_tab_worker::parsetp_start()
{
	mythread->start();
	mythread->thread_function.append("parsetp");
}

void tuning_tab_worker::parsetp_done()
{
	dvb->emit_update_status("", STATUS_CLEAR);
	dvb->emit_update_status("Parsing transponder done", 5);
}
