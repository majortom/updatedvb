/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DVR_THREAD_H
#define DVR_THREAD_H

#include <QDebug>
#include <QThread>
#include <QByteArray>
#include <unistd.h>
#include <fcntl.h>
#include "dvbtune_thread.h"

class dvbtune_thread;

class dvr_thread : public QThread
{
	Q_OBJECT
public:
	dvr_thread();
	~dvr_thread();

	void run();
	bool loop;
	QVector<QString> thread_function;

	void demux_file();
	void demux_stream();
	void close_file();

	QString file_name;
	int file_fd;
	dvbtune_thread *mydvbtune_thread;

signals:
	void data(QByteArray);
	void data_size(int);
};

#endif // DVR_THREAD_H
