/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "views/demux_file_window.h"
#include "ui_demux_file_window.h"

demux_file_window::demux_file_window(dvb_class *d) :
	ui(new Ui::demux_file_window)
{
	ui->setupUi(this);
	dvb = d;

	mystatus = new QStatusBar;
	ui->verticalLayout->addWidget(mystatus);
	mystatus->setVisible(true);
	ui->pushButton_start->setEnabled(true);
	ui->pushButton_stop->setEnabled(false);
}

demux_file_window::~demux_file_window()
{
	on_pushButton_stop_clicked();

	delete mystatus;
	delete ui;
}

void demux_file_window::init()
{
	connect(dvb->dvb_thread->mydvr, &dvr_thread::data_size, this, &demux_file_window::demux_status);

	this->setWindowTitle("Save TS to file...");
	ui->lineEdit_filename->setText(filename);
}

void demux_file_window::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Escape) {
		this->close();
	}
	if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
		on_pushButton_start_clicked();
	}
}

void demux_file_window::on_pushButton_start_clicked()
{
	ui->pushButton_stop->setEnabled(true);
	ui->pushButton_start->setEnabled(false);
	bytes_wrote = 0;
	dvb->dvb_thread->out_name = ui->lineEdit_filename->text();
	dvb->dvb_thread->demux_file(true);
}

void demux_file_window::on_pushButton_stop_clicked()
{
	ui->pushButton_start->setEnabled(true);
	ui->pushButton_stop->setEnabled(false);
	dvb->dvb_thread->demux_file(false);
}

void demux_file_window::demux_status(int bytes)
{
	bytes_wrote += bytes;
	mystatus->showMessage(QString("%L1 KB").arg(bytes_wrote/1000), 0);
}

void demux_file_window::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event);
	this->deleteLater();
}

void demux_file_window::on_pushButton_browse_clicked()
{
	dvb->dvb_thread->out_name = ui->lineEdit_filename->text();
	dvb->dvb_thread->out_name = QFileDialog::getSaveFileName(this, "Save Parsed Output", dvb->dvb_thread->out_name);
	if (dvb->dvb_thread->out_name.isEmpty()) {
		return;
	}
	ui->lineEdit_filename->setText(dvb->dvb_thread->out_name);
}
