/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dvb_class.h"

dvb_class::dvb_class(master_class *m)
{
	mc = m;

	for (unsigned int l = 0; l < MAX_LNBS; l++) {
		mc->qwt_lnb.append(new qwt_data());
	}

	dvb_thread = new dvbtune_thread(mc);
	connect(dvb_thread, &dvbtune_thread::update_status, this, &dvb_class::emit_update_status);
}

dvb_class::~dvb_class()
{
}

void dvb_class::emit_update_status(QString text, int time)
{
	emit update_status(text, time);
}
