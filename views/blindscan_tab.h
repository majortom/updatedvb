/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLINDSCAN_TAB_H
#define BLINDSCAN_TAB_H

#include <QWidget>
#include <QtCore>
#include <QProcess>
#include <QProgressBar>
#include <QDebug>
#include <QStatusBar>
#include "classes/freq_list_class.h"
#include "threads/blindscan_tab_thread.h"
#include "views/tuning_tab.h"
#include "views_workers/blindscan_tab_worker.h"

namespace Ui {
class blindscan_tab;
}

class blindscan_tab : public QWidget
{
	Q_OBJECT
public:
	explicit blindscan_tab(QWidget *parent, dvb_class *d);
	~blindscan_tab();
	void setup_freq();
	void scan();
	void smartscan();

	dvb_class *dvb;

	blindscan_tab_worker *worker;

signals:
	void tab_closed(int index);
	void ctrl_tab();
	void new_tuning_tab();

private slots:
	void on_pushButton_tune_clicked();
	void on_pushButton_save_clicked();
	void on_pushButton_expand_clicked();
	void on_pushButton_unexpand_clicked();
	void on_pushButton_stopstart_clicked();

	void update_signal();
	void update_progress(int i);

private:
	Ui::blindscan_tab *ui;

	void start_scan();
	int tree_create_root(QString text);
	int tree_create_child(int parent, QString text);

	blindscan_tab_thread *mythread;
	int pindex;
	int cindex;
	QTreeWidgetItem *ptree[65535];
	QTreeWidgetItem *ctree[65535];
	dvb_settings dvbnames;
	QVector<tp_info> mytp_info;
	QPointer<QProgressBar> myprogress;

	QString type;

protected:
	void closeEvent(QCloseEvent *event);
	void keyPressEvent(QKeyEvent *event);
};

#endif // BLINDSCAN_TAB_H
