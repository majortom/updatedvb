/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLINDSCAN_TAB_WORKER_H
#define BLINDSCAN_TAB_WORKER_H

#include <QtCore>
#include <QObject>
#include "classes/dvb_class.h"

class blindscan_tab_worker : public QObject
{
	Q_OBJECT
public:
	blindscan_tab_worker(dvb_class *d);
	~blindscan_tab_worker();

	dvb_class *dvb;

	void setup_freq();
};

#endif // BLINDSCAN_TAB_WORKER_H
