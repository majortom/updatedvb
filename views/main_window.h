/*	
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QString>
#include "classes/dvb_class.h"
#include "classes/master_class.h"
#include "views/adapter_tab.h"

namespace Ui {
class main_window;
}

class main_window : public QMainWindow
{
	Q_OBJECT
public:
	explicit main_window(QWidget *parent = 0);
	~main_window();

public slots:
	void update_status(QString text, int time);
	void remove_status(QString text);
	void status_set_timer(QString text, int time);

private slots:
	void on_actionSettings_triggered();
	void on_actionExit_triggered();
	void on_actionSave_Screenshot_triggered();

	void on_tabWidget_currentChanged(int index);

private:
	Ui::main_window *ui;
	master_class *mc;

	QVector<QString> mystatus;
	QVector<adapter_tab*> myadapter_tab;

	QSignalMapper status_mapper;
	QTimer *status_timer;

	QVector<int> get_adapters();
	void name_tabs();

	bool shutdown;

protected:
	void closeEvent(QCloseEvent *event);
};

#endif // MAIN_WINDOW_H
