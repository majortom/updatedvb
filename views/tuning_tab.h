/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUNING_TAB_H
#define TUNING_TAB_H

#include <QWidget>
#include <QtCore>
#include <QProcess>
#include <QThread>
#include <QDebug>
#include <QTreeWidget>
#include <QListWidget>
#include <QStatusBar>
#include <QThread>
#include <QFileDialog>
#include <QMessageBox>
#include <QKeyEvent>
#include <QSemaphore>
#include "classes/dvb_class.h"
#include "views_workers/tuning_tab_worker.h"

namespace Ui {
class tuning_tab;
}

class tuning_tab : public QWidget
{
	Q_OBJECT
public:
	explicit tuning_tab(QWidget *parent, dvb_class *d);
	~tuning_tab();
	void update_signal();

	dvb_class *dvb;

	bool shutdown;
	tuning_tab_worker *worker;

signals:
	void adapter_status(int adapter);
	void new_iqplot_tab();
	void tab_closed(int index);
	void ctrl_tab();

private slots:
	void update_results();
	void on_pushButton_play_clicked();
	void on_pushButton_demux_clicked();
	void on_pushButton_savets_clicked();
	void on_pushButton_expand_clicked();
	void on_pushButton_minimize_clicked();
	void on_pushButton_httpstream_clicked();
	void on_pushButton_strength_clicked();
	void on_pushButton_quality_clicked();
	void on_pushButton_iqplot_clicked();
	void on_pushButton_save_tree_clicked();
	void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);
	void on_listWidget_itemClicked(QListWidgetItem *item);
	void list_create();
	void tree_create(tree_item *item);
	void tree_select_children(QTreeWidgetItem *item);
	void save_children(QTreeWidgetItem *item, QTextStream *out);
	void set_tree_color(QTreeWidgetItem *item, QColor color);

private:
	Ui::tuning_tab *ui;
	QVector<tree_item> tree_items;
	QVector<int> list_pid;
	QVector<QListWidgetItem *> list_item;
	QTime unlock_t;
	dvb_settings dvbnames;

	void setup_demux();

protected:
	void closeEvent(QCloseEvent *event);
	void keyPressEvent(QKeyEvent *event);
};

#endif // TUNING_TAB_H
