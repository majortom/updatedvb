/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "blindscan_tab_thread.h"

blindscan_tab_thread::blindscan_tab_thread(dvb_class *d)
{
	dvb = d;
	loop = false;
}

blindscan_tab_thread::~blindscan_tab_thread()
{
}

void blindscan_tab_thread::run()
{
	loop = true;
	do {
		if (thread_function.contains("blindscan")) {
			switch(dvb->dvb_thread->tune_ops.voltage) {
			case 0:
			case 1:
			case 2:
				dvb->dvb_thread->tp.polarity = dvb->dvb_thread->tune_ops.voltage;
				blindscan();
				loop = false;
				break;
			case 3:
				dvb->dvb_thread->tp.polarity = SEC_VOLTAGE_13;
				blindscan();
				dvb->dvb_thread->tp.polarity = SEC_VOLTAGE_18;
				blindscan();
				loop = false;
				break;
			}
		}
		if (thread_function.contains("smartscan")) {
			smartscan();
			loop = false;
		}
		if (thread_function.isEmpty() && loop) {
			msleep(100);
		}
	} while (loop);
	thread_function.clear();
}

void blindscan_tab_thread::smartscan()
{
	qDebug() << Q_FUNC_INFO;

	dvb->dvb_thread->setbit(TUNER_SCAN);
	QTime t;
	t.start();

	float div  = 100.0 / dvb->dvb_thread->tp_try.size();
	float prog = 0;
	for (tp_info t : dvb->dvb_thread->tp_try) {
		if (!loop) {
			dvb->dvb_thread->unsetbit(TUNER_SCAN);
			return;
		}
		dvb->dvb_thread->tp.frequency	= t.frequency;
		dvb->dvb_thread->tp.polarity	= t.polarity;
		dvb->dvb_thread->tp.system	= t.system;
		dvb->dvb_thread->tp.symbolrate	= dvb->dvb_thread->tune_ops.symbolrate;
		mutex.lock();
		dvb->dvb_thread->tune();
		mutex.wait(&loop);
		prog += div;
		emit update_progress(prog);
	}
	emit update_progress(100);
	qDebug() << "Total time: " << t.elapsed() << ", Frequencies scanned: " << dvb->dvb_thread->tp_try.size();
	dvb->dvb_thread->unsetbit(TUNER_SCAN);
}

void blindscan_tab_thread::blindscan()
{
	qDebug() << Q_FUNC_INFO;

	int rolloff;
	int step, step_tp, step_bw;
	dvb->dvb_thread->setbit(TUNER_SCAN);
	tp_info base_tp;
	QTime t;
	t.start();

	float size = abs(dvb->dvb_thread->tune_ops.f_start-dvb->dvb_thread->tune_ops.f_stop) - 18;
	dvb->dvb_thread->tp.frequency = dvb->dvb_thread->tune_ops.f_start;
	base_tp = dvb->dvb_thread->tp;
	step_bw = (base_tp.symbolrate * 1.35) / 2;

	while (dvb->dvb_thread->tp.frequency < dvb->dvb_thread->tune_ops.f_stop) {
		if (!loop) {
			dvb->dvb_thread->unsetbit(TUNER_SCAN);
			return;
		}
		mutex.lock();
		dvb->dvb_thread->tune();
		mutex.wait(&loop);

		switch(dvb->dvb_thread->tp.rolloff) {
		case 1:
			rolloff = 1.20;
			break;
		case 2:
			rolloff = 1.25;
			break;
		case 0:
		default:
			rolloff = 1.35;
			break;
		}

		step_tp = (dvb->dvb_thread->tp.symbolrate * rolloff) / 2;
		if (dvb->dvb_thread->tp.status & FE_HAS_LOCK) {
			step = step_bw + step_tp;
		} else {
			step = step_bw;
		}
		if (step < 1000) {
			step = 1000;
		}
		base_tp.frequency += (step / 1000);
		dvb->dvb_thread->tp = base_tp;

		int progress = ((dvb->dvb_thread->tp.frequency-dvb->dvb_thread->tune_ops.f_start)/size)*100;
		emit update_progress(progress);
	}
	qDebug() << "Total time: " << t.elapsed();
	dvb->dvb_thread->unsetbit(TUNER_SCAN);
}
