/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "helper_classes.h"

qwt_polarity::qwt_polarity()
{
	qwt_curve = new QwtPlotCurve;
	qwt_curve->setRenderHint(QwtPlotItem::RenderAntialiased);
	f_start = 0;
	f_stop  = 0;
	min     = 0;
	max     = 0;
}

qwt_polarity::~qwt_polarity()
{
	for(QwtPlotMarker *t : qwt_marker) {
		t->detach();
		delete t;
	}
	qwt_curve->detach();
	delete qwt_curve;
}

void qwt_polarity::set_color(QwtPlot *plot, QColor c)
{
	QwtText label;
	qwt_curve->setPen(QPen(c));
	for(QwtPlotMarker *t : qwt_marker) {
		t->setLinePen(QPen(c));
		t->detach();
		t->setSymbol(new QwtSymbol(QwtSymbol::Diamond, QBrush(c), QPen(c), QSize(5,5)));
		label = t->label();
		label.setColor(c);
		t->setLabel(label);
		t->attach(plot);
	}
	qwt_curve->detach();
	qwt_curve->attach(plot);
}


void qwt_polarity::hide()
{
	qwt_curve->hide();
	for (QwtPlotMarker *t : qwt_marker)
		t->hide();
}

void qwt_polarity::show()
{
	qwt_curve->show();
	for (QwtPlotMarker *t : qwt_marker)
		t->show();
}

qwt_data::qwt_data()
{
	qwt_pol.append(new qwt_polarity()); // V
	qwt_pol.append(new qwt_polarity()); // H
	qwt_pol.append(new qwt_polarity()); // N
}

qwt_data::~qwt_data()
{
	for (qwt_polarity *t : qwt_pol) {
		delete t;
	}
}

void qwt_data::hide()
{
	for (qwt_polarity *t : qwt_pol) {
		t->hide();
	}
}

void qwt_data::show(int p)
{
	if (p == 3) { // B
		qwt_pol.at(0)->show(); // V
		qwt_pol.at(1)->show(); // H
	} else {
		qwt_pol.at(p)->show();
	}
}

switch_settings::switch_settings()
{
	voltage		= -1;
	tone		= -1;
	committed	= -1;
	uncommitted	= -1;
}

waitout::waitout()
{
	ready = true;
}

void waitout::lock()
{
	ready = false;
}

void waitout::unlock()
{
	ready = true;
}

void waitout::wait()
{
	while (!ready) {
		msleep(10);
	}
}

void waitout::wait(bool *loop)
{
	while (!ready && *loop) {
		msleep(10);
	}
}

tp_info::tp_info()
{
	frequency	= 950;
	polarity	= SEC_VOLTAGE_18;
	symbolrate	= 1000;
	fec		= FEC_AUTO;
	system		= SYS_DVBS;
	modulation	= QPSK;
	inversion	= INVERSION_AUTO;
	rolloff		= ROLLOFF_AUTO;
	pilot		= PILOT_AUTO;
	matype		= 0;
	frame_len	= 0;
	ber		= 0;
	ucb		= 0;
	snr		= 0;
	lvl		= 0;
	spectrumscan_lvl= 0;
	status		= 0;
	ber_scale	= FE_SCALE_COUNTER;
	snr_scale	= FE_SCALE_DECIBEL;
	lvl_scale	= FE_SCALE_DECIBEL;
}

tuning_options::tuning_options()
{
	f_start		= 3700;
	f_stop		= 4200;
	f_lof		= -5150;
	voltage		= SEC_VOLTAGE_18;
	tone		= SEC_TONE_OFF;
	mis		= -1;
	committed	= 0;
	uncommitted	= 0;
	site_lat	= 0;
	site_long	= 0;
}

tree_item::tree_item()
{
	text.clear();
	parent			= -1;
	current			= -1;
	color			= QColor(Qt::green);
	expanded		= true;
	return_parent	= true;
	pid				= 0xFFFF;
	table			= 0xFFFF;
}

void tree_item::save()
{
	saved = parent;
}

void tree_item::restore()
{
	parent = saved;
}

bool isSatellite(int system)
{
	bool ret = false;
	switch(system) {
	case SYS_DCII:
	case SYS_DSS:
	case SYS_DVBS:
	case SYS_DVBS2:
	case SYS_TURBO:
		ret = true;
	}
	return ret;
}

bool isATSC(int system)
{
	bool ret = false;
	switch(system) {
	case SYS_ATSC:
	case SYS_ATSCMH:
		ret = true;
	}
	return ret;
}

bool isVectorATSC(QVector<int> system)
{
	bool ret = false;
	for (int t : system) {
		if (isATSC(t)) {
			ret = true;
		}
	}
	return ret;
}

bool isQAM(int system)
{
	bool ret = false;
	switch(system) {
	case SYS_DVBC_ANNEX_B:
		ret = true;
	}
	return ret;
}

bool isVectorQAM(QVector<int> system)
{
	bool ret = false;
	for (int t : system) {
		if (isQAM(t)) {
			ret = true;
		}
	}
	return ret;
}

bool isDVBT(int system)
{
	bool ret = false;
	switch(system) {
	case SYS_DVBT:
	case SYS_DVBT2:
		ret = true;
	}
	return ret;
}

bool isVectorDVBT(QVector<int> system)
{
	bool ret = false;
	for (int t : system) {
		if (isDVBT(t)) {
			ret = true;
		}
	}
	return ret;
}

int azero(int num)
{
	if (num < 0) {
		num = 0;
	}
	return num;
}

QString tohex(unsigned long int val, int length)
{
	return QString("0x%1").arg(val, length, 16, QChar('0'));
}

bool dvb_pids_containts(QVector<dvb_pids> vec, dvb_pids elm) {
	for (dvb_pids t : vec) {
		if (t.pid == elm.pid && t.tbl == elm.tbl && t.msk == elm.msk) { // Ignore timeout
			return true;
		}
	}
	return false;
}

QString format_freq(int frequency, int system)
{
	QString ret_string = QString::number(frequency);
	freq_list myfreq;

	if (isATSC(system)) {
		myfreq.atsc();
	}
	if (isQAM(system)) {
		myfreq.qam();
	}
	if (isDVBT(system)) {
		myfreq.dvbt();
	}
	if (myfreq.freq.contains(frequency)) {
		ret_string = QString::number(frequency/1000) + "mhz, ch " + QString::number(myfreq.ch.at(myfreq.freq.indexOf(frequency)));
	}
	return ret_string;
}

