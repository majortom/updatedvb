/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IQPLOT_TAB_H
#define IQPLOT_TAB_H

#include <QWidget>
#include <QKeyEvent>
#include <QSettings>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_symbol.h>
#include <qwt_plot_scaleitem.h>
#include <qwt_scale_engine.h>
#include <qwt_plot_marker.h>
#include "classes/dvb_class.h"

const unsigned int MAX_GRADIANT = 6;

namespace Ui {
class iqplot_tab;
}

class iqplot_tab : public QWidget
{
	Q_OBJECT
public:
	explicit iqplot_tab(QWidget *parent, dvb_class *d);
	~iqplot_tab();

	dvb_class *dvb;

signals:
	void tab_closed(int index);
	void ctrl_tab();

private slots:
	void on_pushButton_onoff_clicked();
	void on_comboBox_mode_currentIndexChanged(int index);
	void on_comboBox_point_currentIndexChanged(int index);
	void iqdraw(QVector<short int> x, QVector<short int> y);

private:
	Ui::iqplot_tab *ui;
	QwtPlotCurve *curve[MAX_GRADIANT];
	QwtPlotScaleItem *scaleX;
	QwtPlotScaleItem *scaleY;
	QwtLinearScaleEngine *scale_eng;
	QwtSymbol *scatter_symbol[MAX_GRADIANT];
	QVector<QString> modcod_name;
	QVector<QwtPlotMarker *> modcod_marker;

	void erase();

protected:
	void closeEvent(QCloseEvent *event);
	void keyPressEvent(QKeyEvent *event);
};

#endif // IQPLOT_TAB_H
