/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tuning_tab.h"
#include "ui_tuning_tab.h"

tuning_tab::tuning_tab(QWidget *parent, dvb_class *d) :
	QWidget(parent),
	ui(new Ui::tuning_tab)
{
	ui->setupUi(this);

	dvb = d;

	QPalette black_palette;
	black_palette = ui->listWidget->palette();
	black_palette.setColor(QPalette::Base, Qt::black);
	ui->listWidget->setPalette(black_palette);
	ui->listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
	ui->listWidget->setMinimumSize(230,0);

	ui->label_strength->hide();
	ui->label_quality->hide();

	ui->treeWidget->setColumnCount(1);
	ui->treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
	black_palette = ui->treeWidget->palette();
	black_palette.setColor(QPalette::Base, Qt::black);
	ui->treeWidget->setPalette(black_palette);

	list_pid.clear();
	list_item.clear();
	for (int i = 0; i <= 0x2000; i++) {
		list_pid.append(0);
		list_item.append(new QListWidgetItem(ui->listWidget));
		list_item.last()->setHidden(true);
	}

	unlock_t.start();

	worker = new tuning_tab_worker(dvb);
	connect(dvb->dvb_thread, &dvbtune_thread::update_results, this, &tuning_tab::update_results);
	connect(worker->mythread, &tuning_tab_thread::list_create, this, &tuning_tab::list_create);
	connect(worker->mythread, &tuning_tab_thread::tree_create, this, &tuning_tab::tree_create);

	dvb->emit_update_status("Tuning...", STATUS_NOEXP);
	emit adapter_status(dvb->dvb_thread->adapter);

	this->setWindowTitle("Tuning Adapter " + QString::number(dvb->dvb_thread->adapter) + ", Frontend " + QString::number(dvb->dvb_thread->frontend) + " : " + dvb->dvb_thread->name);

	ui->pushButton_demux->setEnabled(false);
	ui->pushButton_savets->setEnabled(false);
	ui->pushButton_play->setEnabled(false);
	ui->pushButton_httpstream->setEnabled(false);
	ui->pushButton_iqplot->setEnabled(false);
}

tuning_tab::~tuning_tab()
{
	dvb->emit_update_status("", STATUS_CLEAR);
	worker->deleteLater();
}

void tuning_tab::keyPressEvent(QKeyEvent *event)
{
	if (event->modifiers() == Qt::CTRL && event->key() == Qt::Key_Tab) {
		emit ctrl_tab();
	}
	if (event->key() == Qt::Key_Escape) {
		emit tab_closed(-1);
	}
}

void tuning_tab::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event);

	// You have to give it some index
	emit tab_closed(-1);
}

void tuning_tab::tree_select_children(QTreeWidgetItem *item)
{
	for (int a = 0; a < item->childCount(); a++) {
		item->child(a)->setSelected(item->isSelected());
		tree_select_children(item->child(a));
	}
}

void tuning_tab::on_treeWidget_itemClicked(QTreeWidgetItem * item, int column)
{
	Q_UNUSED(column);

	for (QListWidgetItem *t : list_item) {
		t->setSelected(false);
	}

	tree_select_children(item);

	for (int a = 0; a < tree_items.size(); a++) {
		if (tree_items.at(a).tree->isSelected()) {
			if (tree_items.at(a).pid != 0xFFFF) {
				list_item.at(tree_items.at(a).pid)->setSelected(item->isSelected());
			}
		}
	}
}

void tuning_tab::on_listWidget_itemClicked(QListWidgetItem *item)
{
	Q_UNUSED(item);

	for (tree_item t : tree_items) {
		t.tree->setSelected(false);
	}

	for (int a = 0; a < list_item.size(); a++) {
		if (list_item.at(a)->isSelected()) {
			for (int i = 0; i < tree_items.size(); i++) {
				if (tree_items.at(i).pid == (unsigned int)a) {
					tree_items.at(i).tree->setSelected(true);
				}
			}
		}
	}
}

void tuning_tab::on_pushButton_play_clicked()
{
	setup_demux();
	worker->demux_play();
}

void tuning_tab::on_pushButton_demux_clicked()
{
	setup_demux();
	worker->demux_dvr();
}

void tuning_tab::on_pushButton_savets_clicked()
{
	ui->pushButton_savets->setEnabled(false);

	setup_demux();
	worker->demux_save(ui->label_frequency->text().replace(" ", "_").replace(",", "") + ".ts");
}

void tuning_tab::on_pushButton_httpstream_clicked()
{
	if (worker->mystream->server && worker->mystream->server->isListening())
	{
		worker->demux_https_disconnect();
	} else {
		setup_demux();
		worker->demux_http();
	}
}

void tuning_tab::on_pushButton_expand_clicked()
{
	for (tree_item t : tree_items) {
		t.tree->setExpanded(true);
	}
}

void tuning_tab::on_pushButton_minimize_clicked()
{
	for (tree_item t : tree_items) {
		t.tree->setExpanded(false);
	}
}

void tuning_tab::on_pushButton_iqplot_clicked()
{
	emit new_iqplot_tab();
}

void tuning_tab::on_pushButton_save_tree_clicked()
{
	static QString filename = QFileDialog::getSaveFileName(this, "Save Parsed Output", dvb->mc->mysettings->value("savelocation").toString() + "/" + ui->label_frequency->text().replace(" ", "_").replace(",", "") + ".txt");
	if (filename.isEmpty()) {
		return;
	}
	QFile output(filename);
	output.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&output);

	for (tree_item t : tree_items) {
		if (t.parent == -1) {
			save_children(t.tree, &out);
		}
	}

	QMessageBox mbox;
	mbox.setText("Parsed tree output saved");
	mbox.exec();
	output.close();
}

void tuning_tab::on_pushButton_strength_clicked()
{
	if (ui->label_strength->isVisible()) {
		ui->label_strength->hide();
	} else {
		ui->label_strength->show();
	}
}

void tuning_tab::on_pushButton_quality_clicked()
{
	if (ui->label_quality->isVisible()) {
		ui->label_quality->hide();
	} else {
		ui->label_quality->show();
	}
}

void tuning_tab::save_children(QTreeWidgetItem *item, QTextStream *out)
{
	static QString indent;
	*out << indent << item->text(0) << "\n";
	for (int i = 0; i < item->childCount(); i++) {
		indent.append("\t");
		save_children(item->child(i), out);
		indent.chop(1);
	}
}

void tuning_tab::update_signal()
{
	// large font for Quality, dB
	QFont dBfont;
	dBfont.setPointSize(20);
	dBfont.setBold(true);

	QString danger = "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 #FF0350,stop: 0.4999 #FF0020,stop: 0.5 #FF0019,stop: 1 #FF0000 );border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;border: .px solid black;}";
	QString safe   = "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 #78d,stop: 0.4999 #46a,stop: 0.5 #45a,stop: 1 #238 );border-bottom-right-radius: 7px;border-bottom-left-radius: 7px;border: 1px solid black;}";

	QString lvl = QString::number(dvb->dvb_thread->tp.lvl, 'f', 1);
	ui->progressBar_strength->setStyleSheet(safe);
	if (dvb->dvb_thread->tp.lvl_scale == FE_SCALE_DECIBEL) {
		ui->progressBar_strength->setMinimum(-100);
		ui->progressBar_strength->setMaximum(0);
		ui->progressBar_strength->setFormat("%v dBm");
		ui->label_strength->setText(QString("<b><font size=8>%1</font>dBm</b>").arg(lvl));
	} else {
		ui->progressBar_strength->setMinimum(0);
		ui->progressBar_strength->setMaximum(100);
		ui->progressBar_strength->setFormat("%p%");
		ui->label_strength->setText(QString("<font size=8><b>%1%</b></font>").arg(lvl));
	}
	ui->progressBar_strength->setValue(dvb->dvb_thread->tp.lvl);

	QString min = QString::number(dvb->dvb_thread->min_snr(), 'f', 1);
	QString snr = QString::number(dvb->dvb_thread->tp.snr, 'f', 1);
	ui->progressBar_quality->setStyleSheet(safe);
	if (dvb->dvb_thread->tp.snr_scale == FE_SCALE_DECIBEL) {
		if(ui->progressBar_quality->value() < dvb->dvb_thread->min_snr()) {
			ui->progressBar_quality->setStyleSheet(danger);
		}
		if (dvb->dvb_thread->tp.snr > ui->progressBar_quality->maximum()) {
			ui->progressBar_quality->setMaximum(dvb->dvb_thread->tp.snr * 1.5);
		}
		ui->progressBar_quality->setMinimum(0);
		ui->progressBar_quality->setFormat("%v dB");
		ui->label_quality->setText(QString("min:<b><font size=8>%1</font>dB</b><br>cur:<b><font size=8>%2</font>dB</b>").arg(min).arg(snr));
	} else {
		ui->progressBar_quality->setMinimum(0);
		ui->progressBar_quality->setMaximum(100);
		ui->progressBar_quality->setFormat("%p%");
		ui->label_quality->setText(QString("<font size=8><b>%1%</b></font>").arg(snr));
	}
	ui->progressBar_quality->setValue(round(dvb->dvb_thread->tp.snr));

	if (dvb->dvb_thread->tp.status & FE_HAS_LOCK) {
		if (worker->parsetp_started) {
			unlock_t.restart();
		} else if (unlock_t.elapsed() > 5000) {
			update_results();
		}
		ui->pushButton_demux->setEnabled(true);
		if (worker->mydemux_file_window.isNull()) {
			ui->pushButton_savets->setEnabled(true);
		}
		ui->pushButton_play->setEnabled(true);
		ui->pushButton_httpstream->setEnabled(true);
		ui->label_lock->setText("Locked");
		ui->label_lock->setStyleSheet("QLabel { color : lime; }");
	} else {
		unlock_t.restart();
		ui->pushButton_demux->setEnabled(false);
		ui->pushButton_savets->setEnabled(false);
		ui->pushButton_play->setEnabled(false);
		ui->pushButton_httpstream->setEnabled(false);
		ui->label_lock->setText("Unlocked");
		ui->label_lock->setStyleSheet("QLabel { color : red; }");
		if (isSatellite(dvb->dvb_thread->tp.system) && dvb->dvb_thread->tp.fec == FEC_AUTO) {
			ui->label_frequency->setText("");
			return;
		}
	}
	if (dvb->dvb_thread->extended_caps & FE_CAN_IQ) {
		ui->pushButton_iqplot->setEnabled(true);
	} else {
		ui->pushButton_iqplot->setEnabled(false);
	}

	ui->label_ber->setText(QString::number(dvb->dvb_thread->tp.ber));
	ui->label_system->setText(dvbnames.system(dvb->dvb_thread->tp.system));
	ui->label_modulation->setText(dvbnames.modulation(dvb->dvb_thread->tp.modulation));
	if(isATSC(dvb->dvb_thread->tp.system)) {
		float bw = 6;
		ui->label_bw->setText(QString::number(bw) +" MHz");
	}
	if (isSatellite(dvb->dvb_thread->tp.system)) {
		ui->label_frequency->setText(QString::number(dvb->dvb_thread->tp.frequency) + dvbnames.voltage[dvb->dvb_thread->tp.polarity] + QString::number(dvb->dvb_thread->tp.symbolrate));
		ui->label_fec->setText(dvbnames.fec(dvb->dvb_thread->tp.fec));
		ui->label_inversion->setText(dvbnames.inversion(dvb->dvb_thread->tp.inversion));
		ui->label_rolloff->setText(dvbnames.rolloff(dvb->dvb_thread->tp.rolloff));
		ui->label_pilot->setText(dvbnames.pilot(dvb->dvb_thread->tp.pilot));
		float ro= dvbnames.rolloff(dvb->dvb_thread->tp.rolloff).toDouble()/100;
		float bw= (dvb->dvb_thread->tp.symbolrate * (1 + ro))/1000;
		ui->label_bw->setText(QString::number(bw) +" MHz");

		ui->label_framelength->setText(dvb->dvb_thread->tp.frame_len ? "Short" : "Long");

		if (dvb->dvb_thread->tp.matype > 0) {
			unsigned int matype = dvb->dvb_thread->tp.matype >> 8;
			switch(dvb->dvb_thread->maskbits(matype, 0xC0)) {
			case 0:
				ui->label_tsgs->setText("GP");
				break;
			case 1:
				ui->label_tsgs->setText("GS");
				break;
			case 2:
				ui->label_tsgs->setText("Reserved");
				break;
			case 3:
				ui->label_tsgs->setText("TS");
				break;
			}
			switch(dvb->dvb_thread->maskbits(matype, 0x20)) {
			case 0:
				ui->label_sismis->setText("MIS");
				break;
			case 1:
				ui->label_sismis->setText("SIS");
				break;
			}
			switch(dvb->dvb_thread->maskbits(matype, 0x10)) {
			case 0:
				ui->label_ccmacm->setText("ACM/VCM");
				break;
			case 1:
				ui->label_ccmacm->setText("CCM");
				break;
			}
		}

		ui->gridWidget_satellite->setVisible(true);
	} else {
		ui->label_frequency->setText(format_freq(dvb->dvb_thread->tp.frequency, dvb->dvb_thread->tp.system));
		ui->gridWidget_satellite->setVisible(false);
	}
}

void tuning_tab::list_create()
{
	for (int i = 0; i < list_pid.size(); i++) {
		if (dvb->dvb_thread->pids_rate.at(i) > 0) {
			list_pid[i] = dvb->dvb_thread->pids_rate.at(i);
			list_item.at(i)->setHidden(false);
			list_item.at(i)->setText(QString("0x%1 (%2) - %3 kbit/s").arg(i,4,16,QChar('0')).arg(i,4,10,QChar('0')).arg(dvb->dvb_thread->pids_rate.at(i),5,10,QChar(' ')));
			list_item.at(i)->setTextColor(QColor(Qt::gray));
			if (i == 0x1fff || i == 0x2000) {
				list_item.at(i)->setTextColor(QColor(Qt::green));
			}
		} else {
			list_item.at(i)->setHidden(true);
		}
	}
	for (tree_item t : tree_items) { // 0x1fff padding packets and 0x2000 entire mux should show as green
		if (t.pid != 0xFFFF) {
			list_item.at(t.pid)->setTextColor(t.tree->textColor(0));
		}
	}
}

void tuning_tab::set_tree_color(QTreeWidgetItem *item, QColor color)
{
	for (int i = 0; i < item->childCount(); i++) {
		set_tree_color(item->child(i), color);
	}
	item->setTextColor(0, color);
}

void tuning_tab::tree_create(tree_item *item)
{
	if (item->pid != 0xFFFF && dvb->dvb_thread->pids_rate.at(item->pid) == 0) {
		dvb->dvb_thread->pids_rate[item->pid] = 1;
	}

	if (item->pid != 0xFFFF) {
		for (int i = 0; i < tree_items.size(); i++) {
			if (item->pid == tree_items.at(i).pid && item->text == tree_items.at(i).text
					&&  (  item->text.startsWith("TDT")
					       || item->text.startsWith("PSIP")
					       || item->text.startsWith("MGT")
					       || item->text.startsWith("STT")
					       )
					) {
				item->parent = i;

				worker->mythread->mutex.unlock();
				return;
			}
		}
	}

	item->current = tree_items.size();
	tree_items.append(*item);
	if (item->parent < 0 || item->parent >= tree_items.size()) { // Shouldnt have to check this, but I cant reproduce the error to find it
		item->parent = -1;
	}
	if (item->parent == -1) { // Root
		tree_items.last().tree = new QTreeWidgetItem(ui->treeWidget);
		tree_items.last().tree->setText(0, item->text);
		tree_items.last().tree->setExpanded(item->expanded);
		tree_items.last().tree->setTextColor(0, item->color);
	} else { // Child
		tree_items.last().tree = new QTreeWidgetItem();
		tree_items.last().tree->setText(0, item->text);
		if (tree_items.at(item->parent).tree->childCount() == 0) { // Can't expand an item with no children
			tree_items.at(item->parent).tree->setExpanded(item->expanded=false);
		} else {
			tree_items.at(item->parent).tree->setExpanded(tree_items.at(item->parent).tree->isExpanded());
		}
		if (item->color == QColor(Qt::red)) {
			tree_item tmp = tree_items.at(item->parent);
			while (tmp.parent != -1) {
				tmp = tree_items.at(tmp.parent);
			}
			set_tree_color(tmp.tree, item->color);
			tree_items.last().tree->setTextColor(0, item->color);
		} else {
			tree_items.last().tree->setTextColor(0, tree_items.at(item->parent).tree->textColor(0));
		}
		tree_items.at(item->parent).tree->addChild(tree_items.last().tree);
	}
	if (item->return_parent) {
		item->parent = item->current;
	}

	worker->mythread->mutex.unlock();
}

void tuning_tab::update_results()
{
	double maxsnr = dvb->dvb_thread->min_snr();
	if (dvb->dvb_thread->tp.snr > maxsnr)
		maxsnr = dvb->dvb_thread->tp.snr;
	maxsnr *= 1.5;
	ui->progressBar_quality->setMaximum(maxsnr);

	if (dvb->dvb_thread->tp.system == SYS_DSS) {
		return;
	}
	if (dvb->dvb_thread->tp.matype > 0 && dvb->dvb_thread->maskbits(dvb->dvb_thread->tp.matype >> 8, 0xC0) != 3) { // maskbits(tp.matype >> 8, 0xC0) == 3 is TS
		return;
	}

	worker->parsetp_started = true;
	worker->mythread->loop	= false;
	worker->mythread->thread_function.append("parsetp");
	worker->mythread->start();

	dvb->emit_update_status("", STATUS_CLEAR);
	dvb->emit_update_status("Parsing transponder...", STATUS_NOEXP);
}

void tuning_tab::setup_demux()
{
	QVector<unsigned int> pids;
	for(int a = 0; a < list_pid.size(); a++) {
		if (list_item.at(a)->isSelected()) {
			pids.append(a);
		}
	}

	if (pids.isEmpty()) {
		pids.append(0x2000);
	}
	if (!pids.contains(0x00)) { // VLC needs the PAT
		pids.append(0x00);
	}

	worker->setup_demux(pids);
}
