/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tuning_tab_thread.h"

bool DUMP_DESC = false;

tuning_tab_thread::tuning_tab_thread(dvb_class *d)
{
	dvb = d;
	loop = false;
	pid_parent.fill(-1, 0x2000);
}

tuning_tab_thread::~tuning_tab_thread()
{
	loop = false;

	while (dvb->dvb_thread->isRunning()) {
		dvb->dvb_thread->loop = false;
		dvb->dvb_thread->quit();
		dvb->dvb_thread->wait(100);
	}
}

void tuning_tab_thread::run()
{
	loop = true;
	do {
		// we want todo this often and always, looks nice when its constantly updating
		dvb->dvb_thread->get_bitrate();
		emit list_create();

		if (thread_function.contains("parsetp")) {
			parsetp();
		}
		if (thread_function.isEmpty() && loop) {
			msleep(100);
		}
	} while (loop);
	thread_function.clear();
}

unsigned int tuning_tab_thread::dtag_convert(unsigned int temp)
{
	int ret = 0;
	ret += ((temp >> 28) & 0x0F) * qPow(10,7);
	ret += ((temp >> 24) & 0x0F) * qPow(10,6);
	ret += ((temp >> 20) & 0x0F) * qPow(10,5);
	ret += ((temp >> 16) & 0x0F) * qPow(10,4);
	ret += ((temp >> 12) & 0x0F) * qPow(10,3);
	ret += ((temp >> 8) & 0x0F) * qPow(10,2);
	ret += ((temp >> 4) & 0x0F) * qPow(10,1);
	ret += ((temp >> 0) & 0x0F) * qPow(10,0);
	return ret;
}

void tuning_tab_thread::tree_create_wait(tree_item *item)
{
	if (!loop) {
		return;
	}
	
	mutex.lock();
	emit tree_create(item);
	mutex.wait(&loop);
}

void tuning_tab_thread::parse_etm(tree_item *item, QString desc)
{
	tree_item orig;
	orig.parent		= item->parent;
	orig.return_parent	= item->return_parent;
	item->parent		= item->current;

	unsigned int num_str = dvb->dvb_thread->read8();
	for (unsigned int i = 0; i < num_str; i++) {
		item->return_parent	= true;
		item->parent		= orig.parent;
		item->text		= QString("Language: %1").arg(dvb->dvb_thread->readstr(3));
		tree_create_wait(item);
		item->return_parent	= false;

		unsigned int num_seg = dvb->dvb_thread->read8();
		for (unsigned int j = 0; j < num_seg; j++) {
			compression_type ct;
			item->text = QString("Compression Type: %1").arg(ct.whatis(dvb->dvb_thread->read8()));
			tree_create_wait(item);
			encoding_mode em;
			item->text = QString("Text Encoding: %1").arg(em.whatis(dvb->dvb_thread->read8()));
			tree_create_wait(item);
			unsigned int num_bytes = dvb->dvb_thread->read8();
			if (num_bytes > 0) {
				item->text = QString("%1: %2").arg(desc).arg(dvb->dvb_thread->readstr(num_bytes));
				tree_create_wait(item);
			}
		}
	}
	item->parent		= orig.parent;
	item->return_parent	= orig.return_parent;
}

void tuning_tab_thread::parse_descriptor(tree_item *item)
{
	tree_item orig;
	orig.parent		= item->parent;
	orig.current		= item->current;
	orig.return_parent	= item->return_parent;
	item->parent		= item->current;

	unsigned int desc_tag = dvb->dvb_thread->read8();
	unsigned int desc_len = dvb->dvb_thread->read8();
	unsigned int desc_end = desc_len + dvb->dvb_thread->index;

	item->text = QString("Descriptor: %1 - %2 Descriptor").arg(tohex(desc_tag,2)).arg(dvbnames.dvb_descriptortag[desc_tag]);
	item->return_parent = true;
	tree_create_wait(item);
	item->return_parent = false;

	if (DUMP_DESC) {
		dump_desc();
	}

	switch(desc_tag) {
	case 0x02: // video_stream_descriptor
	{
		unsigned int tmp;
		tmp = dvb->dvb_thread->read8();
		item->text = QString("Multiple Frame Rate Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x80));
		tree_create_wait(item);
		frame_rate fr;
		item->text = QString("Frame Rate Code: %1").arg(fr.rate.at(dvb->dvb_thread->maskbits(tmp,0x70)));
		tree_create_wait(item);
		item->text = QString("MPEG1 Only Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x04) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Constrained Paramater Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x02) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Still Picture Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x01) ? "true" : "false");
		tree_create_wait(item);
	}
		break;
	case 0x03: // audio_stream_descriptor
	{
		unsigned int tmp = dvb->dvb_thread->read8();
		item->text = QString("Free Format: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x80) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("ID: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x40) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Layer: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x30));
		tree_create_wait(item);
		item->text = QString("Variable Rate Audio: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x08) ? "true" : "false");
		tree_create_wait(item);
	}
		break;
	case 0x05: // registration_descriptor
		item->text = QString("Format Identifier: %1").arg(dvb->dvb_thread->readstr(4));
		tree_create_wait(item);
		break;
	case 0x06: // data_stream_alignment_descriptor
	{
		data_stream_type dst;
		item->text = QString("Type: %1").arg(dst.name.at(dvb->dvb_thread->read8()));
		tree_create_wait(item);
	}
		break;
	case 0x09: // CA Descriptor
	{
		unsigned int sys = dvb->dvb_thread->read16();
		unsigned int pid = dvb->dvb_thread->read16(0x1FFF);
		item->pid	= pid;
		item->color	= QColor(Qt::red);
		item->text	= QString("CA PID: %1 - system_id: %2 (%3)").arg(tohex(pid,4)).arg(tohex(sys,4)).arg(dvbnames.ca_name[sys]);
		tree_create_wait(item);
	}
		break;
	case 0x0b: // system_clock_descriptor
	{
		unsigned int tmp;
		tmp = dvb->dvb_thread->read8();
		item->text = QString("External Clock Reference Indicatior: %1").arg(dvb->dvb_thread->maskbits(tmp,0x80) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Clock Accuracy Integer: %1").arg(dvb->dvb_thread->maskbits(tmp,0x3F));
		tree_create_wait(item);
		tmp = dvb->dvb_thread->read8();
		item->text = QString("Clock Accuracy Exponent: %1").arg(dvb->dvb_thread->maskbits(tmp,0xE0));
		tree_create_wait(item);
	}
		break;
	case 0x0c: // multiplex_buffer_utilization_descriptor
	{
		unsigned int tmp;
		tmp = dvb->dvb_thread->read16();
		item->text = QString("Lower/Uppoer Bound Flags Valid: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x8000) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("LTW Offset Lower Bound: %1 (27mhz/300) clock periods").arg(dvb->dvb_thread->maskbits(tmp, 0x7FFF));
		tree_create_wait(item);
		item->text = QString("LTW Offset Upper Bound: %1 (27mhz/300) clock periods").arg(dvb->dvb_thread->read16(0x7FFF));
		tree_create_wait(item);
	}
		break;
	case 0x0e: // maximum_bitrate_descriptor
		item->text = QString("Maximum Bitrate: %1 kB/sec").arg(dvb->dvb_thread->read24(0x3FFFFF) * 50 / 1000);
		tree_create_wait(item);
		break;
	case 0x10: // smoothing_buffer_descriptor
		item->text = QString("Leak Rate: %1 bits/sec").arg(dvb->dvb_thread->read24(0x3FFFFF)/400);
		tree_create_wait(item);
		item->text = QString("Buffer Size: %1 bytes").arg(dvb->dvb_thread->read24(0x3FFFFF));
		tree_create_wait(item);
		break;
	case 0x11: // STD_descriptor
		item->text = QString("Leak Valid Flag: %1").arg(dvb->dvb_thread->read8(0x01) ? "true" : "false");
		tree_create_wait(item);
		break;
	case 0x0A: // ISO_639_language_descriptor
	{
		audio_type at;
		while (dvb->dvb_thread->index < desc_end) {
			item->text = QString("Language: %1").arg(dvb->dvb_thread->readstr(3));
			tree_create_wait(item);
			item->text = QString("Audio Type: %1").arg(at.whatis(dvb->dvb_thread->read8()));
			tree_create_wait(item);
		}
	}
		break;
	case 0x28: // AVC_video_descriptor
	{
		item->text = QString("Profile IDC: %1").arg(dvb->dvb_thread->read8());
		tree_create_wait(item);
		unsigned int tmp;
		tmp = dvb->dvb_thread->read8();
		item->text = QString("Constraint set0 Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x80) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Constraint set1 Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x40) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Constraint set2 Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x20) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Constraint set3 Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x10) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Constraint set4 Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x08) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Constraint set5 Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x04) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("AVC Compatible Flags: %1").arg(dvb->dvb_thread->maskbits(tmp,0x03));
		tree_create_wait(item);
		item->text = QString("Level IDC: %1").arg(dvb->dvb_thread->read8());
		tree_create_wait(item);
		tmp = dvb->dvb_thread->read8();
		item->text = QString("AVC Still Present: %1").arg(dvb->dvb_thread->maskbits(tmp,0x80) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("AVC 24h Picture: %1").arg(dvb->dvb_thread->maskbits(tmp,0x40) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Frame Packing SEI Not Present Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x20) ? "true" : "false");
		tree_create_wait(item);
	}
		break;
	case 0x2A: // AVC_timing_and_HRD_descriptor
	{
		unsigned int tmp;
		tmp = dvb->dvb_thread->read8();
		item->text = QString("HRD Management Valid Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x80) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Picture And Timing Info Present: %1").arg(dvb->dvb_thread->maskbits(tmp,0x01) ? "true" : "false");
		tree_create_wait(item);
		if (dvb->dvb_thread->maskbits(tmp,0x01)) {
			unsigned int orig_parent = item->parent;
			tmp = dvb->dvb_thread->read8();
			item->text = QString("90khz Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x80) ? "true" : "false");
			item->return_parent = true;
			tree_create_wait(item);
			item->return_parent = false;
			if (dvb->dvb_thread->maskbits(tmp,0x80)) {
				item->text = QString("N: %1").arg(dvb->dvb_thread->read32());
				tree_create_wait(item);
				item->text = QString("K: %1").arg(dvb->dvb_thread->read32());
				tree_create_wait(item);
			}
			item->parent = orig_parent;
		}
		tmp = dvb->dvb_thread->read8();
		item->text = QString("Fixed Frame Rate Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x80) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Temporal POC Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x40) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Picture To Display Conversion Flag: %1").arg(dvb->dvb_thread->maskbits(tmp,0x20) ? "true" : "false");
		tree_create_wait(item);
	}
		break;
	case 0x2b: // MPEG2_AAC_Audio_Descriptor
	{
		AAC aac;
		item->text = QString("%1 Profile").arg(aac.profile.at(dvb->dvb_thread->read8()));
		tree_create_wait(item);
		item->text = QString("Channel Configuration: %1").arg(aac.channel_configuration.at(dvb->dvb_thread->read8()));
		tree_create_wait(item);
	}
		break;
	case 0x40: // network_name_descriptor
		item->text = QString("Network Name: %1").arg(dvb->dvb_thread->readstr(desc_len));
		tree_create_wait(item);
		break;
	case 0x41: // service_list_descriptor
		while(dvb->dvb_thread->index < desc_end) {
			unsigned int sid = dvb->dvb_thread->read16();
			unsigned int type = dvb->dvb_thread->read8();
			item->text = QString("Service ID: %1 - %2").arg(sid).arg(dvbnames.stream_type[type]);
			tree_create_wait(item);
		}
		break;
	case 0x43: // satellite_delivery_system_descriptor
	{
		unsigned int frequency;
		unsigned int symbol_rate;
		unsigned int temp;
		int orbital_pos;
		int east_west_flag;
		int polarization;
		int roll_off;
		int modulation_system;
		int modulation_type;
		int fec_inner;

		frequency = dtag_convert(dvb->dvb_thread->read32());
		orbital_pos = dtag_convert(dvb->dvb_thread->read16());

		temp = dvb->dvb_thread->read8();
		east_west_flag = dvb->dvb_thread->maskbits(temp, 0x80);
		polarization = dvb->dvb_thread->maskbits(temp, 0x60);
		roll_off = dvb->dvb_thread->maskbits(temp, 0x18);
		modulation_system = dvb->dvb_thread->maskbits(temp, 0x04);
		modulation_type = dvb->dvb_thread->maskbits(temp, 0x03);

		temp = dvb->dvb_thread->read32();
		symbol_rate = dtag_convert(dvb->dvb_thread->maskbits(temp, 0xFFFFFFF0));
		fec_inner = dvb->dvb_thread->maskbits(temp, 0x0F);

		item->text = QString("Frequency: %1 %2 %3 , fec = %4").arg(frequency/100).arg(dvbnames.dtag_polarization[polarization]).arg(symbol_rate/10).arg(dvbnames.dtag_fec[fec_inner]);
		tree_create_wait(item);

		item->text = QString("Modulation System: %1").arg(modulation_system ? "DVB-S2" : "DVB-S");
		tree_create_wait(item);

		item->text = QString("Modulation Type: %1").arg(dvbnames.dtag_modulation[modulation_type]);
		tree_create_wait(item);

		if (modulation_system) {
			item->text = QString("Roll Off: %1").arg(dvbnames.dtag_rolloff[roll_off]);
			tree_create_wait(item);
		}
		item->text = QString("Orbital Position: %L1%2").arg(QString::number(orbital_pos/10.0, 'f', 1)).arg(east_west_flag ? "e" : "w");
		tree_create_wait(item);
	}
		break;
	case 0x44: // cable_delivery_system_descriptor
	{
		item->text = QString("Frequency: %1").arg(dtag_convert(dvb->dvb_thread->read32()));
		tree_create_wait(item);

		fec_outer fec_o;
		item->text = QString("FEC Outer: %1").arg(fec_o.whatis(dvb->dvb_thread->read16(0x000f)));
		tree_create_wait(item);

		qam_modulation qam;
		item->text = QString("Modulation: %1").arg(qam.whatis(dvb->dvb_thread->read8()));
		tree_create_wait(item);

		unsigned int temp = dvb->dvb_thread->read32();

		item->text = QString("Symbol Rate: %1").arg(dtag_convert(dvb->dvb_thread->maskbits(temp, 0xfffff0)));
		tree_create_wait(item);

		fec_inner fec_i;
		item->text = QString("FEC Inner: %1").arg(fec_i.whatis(dvb->dvb_thread->maskbits(temp, 0x00000f)));
		tree_create_wait(item);
	}
		break;
	case 0x45: // VBI_data_descriptor
	{
		data_service ds;
		while (dvb->dvb_thread->index < desc_end) {
			unsigned int data_service_id = dvb->dvb_thread->read8();
			item->text = QString("Data Service ID: %1 - %2").arg(data_service_id).arg(ds.text.at(data_service_id));
			tree_create_wait(item);
			unsigned int data_service_descriptor_length = dvb->dvb_thread->read8();
			switch (data_service_id) {
			case 0x01:
			case 0x02:
			case 0x04:
			case 0x05:
			case 0x06:
			case 0x07:
				for (unsigned int i = 0; i < data_service_descriptor_length; i++) {
					unsigned int tmp = dvb->dvb_thread->read8();
					item->text = QString("Parity: %1 field of a frame").arg(dvb->dvb_thread->maskbits(tmp, 0x20) ? "first (odd)" : "second (even)");
					tree_create_wait(item);
					item->text = QString("Line Number: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x1F));
					tree_create_wait(item);
				}
				break;
			default:
				dvb->dvb_thread->index += data_service_descriptor_length;
				break;
			}

		}
	}
		break;
	case 0x48: // service_descriptor
	{
		unsigned int service_type = dvb->dvb_thread->read8();
		dvb_service_type dst;
		item->text = QString("Service Type: %1").arg(dst.whatis(service_type));
		tree_create_wait(item);
		mysdt.pname.append(dvb->dvb_thread->readstr(dvb->dvb_thread->read8()));
		mysdt.sname.append(dvb->dvb_thread->readstr(dvb->dvb_thread->read8()));
		if (!mysdt.sname.last().isEmpty()) {
			item->text = QString("Service Name: %1").arg(mysdt.sname.last());
			tree_create_wait(item);
		}
		if (!mysdt.pname.last().isEmpty()) {
			item->text = QString("Provider Name: %1").arg(mysdt.pname.last());
			tree_create_wait(item);
		}
	}
		break;
	case 0x49: // country_availability_descriptor
	{
		item->text = QString("Availability Flag: %1").arg(dvb->dvb_thread->read8(0x80) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Country Code: %1").arg(dvb->dvb_thread->readstr(desc_len-1));
		tree_create_wait(item);
	}
		break;
	case 0x4d: // short_event_descriptor
	{
		item->text = QString("Language: %1").arg(dvb->dvb_thread->readstr(3));
		tree_create_wait(item);
		unsigned int num_bytes;
		num_bytes = dvb->dvb_thread->read8();
		if (num_bytes > 0) {
			item->text = QString("Event Name: %1").arg(dvb->dvb_thread->readstr(num_bytes));
			tree_create_wait(item);
		}
		num_bytes = dvb->dvb_thread->read8();
		if (num_bytes > 0) {
			item->text = QString("Text: %1").arg(dvb->dvb_thread->readstr(num_bytes));
			tree_create_wait(item);
		}
	}
		break;
	case 0x4e: // extended_event_descriptor
	{
		unsigned int tmp = dvb->dvb_thread->read8();
		item->text = QString("Descriptor Number: %1").arg(dvb->dvb_thread->maskbits(tmp, 0xF0));
		tree_create_wait(item);
		item->text = QString("Last Descriptor Number: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x0F));
		tree_create_wait(item);
		item->text = QString("Country Code: %1").arg(dvb->dvb_thread->readstr(3));
		tree_create_wait(item);
		unsigned int length_of_items = dvb->dvb_thread->read8() + dvb->dvb_thread->index;
		while (dvb->dvb_thread->index < length_of_items) {
			unsigned int item_description_length = dvb->dvb_thread->read8();
			item->text = QString("Item Description: %1").arg(dvb->dvb_thread->readstr(item_description_length));
			tree_create_wait(item);
			unsigned int item_length = dvb->dvb_thread->read8();
			item->text = QString("Item: %1").arg(dvb->dvb_thread->readstr(item_length));
			tree_create_wait(item);
		}
		unsigned int text_length = dvb->dvb_thread->read8();
		item->text = QString("Text: %1").arg(dvb->dvb_thread->readstr(text_length));
		tree_create_wait(item);
	}
		break;
	case 0x50: // component_descriptor
	{
		stream_content sm;
		unsigned int s_type = dvb->dvb_thread->read8(0x0F);
		unsigned int c_type = dvb->dvb_thread->read8();
		item->text = QString("Stream Content: %1, Component Type: %2").arg(s_type).arg(c_type);
		tree_create_wait(item);
		item->text = QString("Stream/Component Description: %1").arg(sm.whatis(s_type, c_type));
		tree_create_wait(item);
		item->text = QString("Component Tag: %1").arg(dvb->dvb_thread->read8());
		tree_create_wait(item);
		item->text = QString("Language: %1").arg(dvb->dvb_thread->readstr(3));
		tree_create_wait(item);
		item->text = QString("Component Text: %1").arg(dvb->dvb_thread->readstr(desc_end - dvb->dvb_thread->index));
		tree_create_wait(item);
	}
		break;
	case 0x52: // stream_identifier_descriptor
		item->text = QString("Component Tag: %1").arg(dvb->dvb_thread->read8());
		tree_create_wait(item);
		break;
	case 0x55: // parental_rating_descriptor
		while(dvb->dvb_thread->index < desc_end) {
			item->text = QString("Country Code: %1").arg(dvb->dvb_thread->readstr(3));
			tree_create_wait(item);
			item->text = QString("Minimum Age: %1").arg(dvb->dvb_thread->read8()+3);
			tree_create_wait(item);
		}
		break;
	case 0x56: // teletext_descriptor
		while(dvb->dvb_thread->index < desc_end) {
			item->text = QString("Language Code: %1").arg(dvb->dvb_thread->readstr(3));
			tree_create_wait(item);
			unsigned int tmp = dvb->dvb_thread->read8();
			teletext_type tt;
			item->text = QString("Teletext Type: %1").arg(tt.text.at(dvb->dvb_thread->maskbits(tmp, 0xF8)));
			tree_create_wait(item);
			item->text = QString("Magazine Number: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x07));
			tree_create_wait(item);
			item->text = QString("Page Number: %1").arg(dvb->dvb_thread->read8());
			tree_create_wait(item);
		}
		break;
	case 0x59: // subtitling_descriptor
		while(dvb->dvb_thread->index < desc_end) {
			item->text = QString("Language Code: %1").arg(dvb->dvb_thread->readstr(3));
			tree_create_wait(item);
			stream_content sm;
			item->text = QString("Stream/Component Description: %1").arg(sm.whatis(0x1, dvb->dvb_thread->read8()));
			tree_create_wait(item);
			item->text = QString("Composition Page ID: %1").arg(dvb->dvb_thread->read16());
			tree_create_wait(item);
			item->text = QString("Ancillary Page ID: %1").arg(dvb->dvb_thread->read16());
			tree_create_wait(item);
		}
		break;
	case 0x5A: // terrestrial_delivery_system_descriptor
	{
		unsigned int tmp = dvb->dvb_thread->read32();
		item->text = QString("Center Frequency: %1hz").arg(tmp);
		tree_create_wait(item);
		tmp = dvb->dvb_thread->read24();
		t_delsys_bandwidth tb;
		item->text = QString("Frequency: %1hz").arg(tb.text.at(dvb->dvb_thread->maskbits(tmp, 0x800000)));
		tree_create_wait(item);
		item->text = QString("Priority: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x400000) ? "High" : "Low");
		tree_create_wait(item);
		item->text = QString("Time Slicing: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x20000) ? "Not Used" : "Used");
		tree_create_wait(item);
		item->text = QString("MPE-FEC: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x10000) ? "Not Used" : "Used");
		tree_create_wait(item);
		t_delsys_constellation tc;
		item->text = QString("Constelation: %1").arg(tc.text.at(dvb->dvb_thread->maskbits(tmp, 0xC0000)));
		tree_create_wait(item);
		t_delsys_hierarchy_information ti;
		item->text = QString("Hierarchy Information: %1").arg(ti.text.at(dvb->dvb_thread->maskbits(tmp, 0x3800)));
		tree_create_wait(item);
		t_delsys_fec tf;
		item->text = QString("Fec HP: %1").arg(tf.text.at(dvb->dvb_thread->maskbits(tmp, 0x700)));
		tree_create_wait(item);
		item->text = QString("Fec LP: %1").arg(tf.text.at(dvb->dvb_thread->maskbits(tmp, 0xE0)));
		tree_create_wait(item);
		t_delsys_guard tg;
		item->text = QString("Guard Interval: %1").arg(tg.text.at(dvb->dvb_thread->maskbits(tmp, 0x18)));
		tree_create_wait(item);
		t_delsys_tmode tt;
		item->text = QString("Transmission Mode: %1").arg(tt.text.at(dvb->dvb_thread->maskbits(tmp, 0x6)));
		tree_create_wait(item);
		item->text = QString("Other Frequencies Used: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x1) ? "Yes" : "No");
		tree_create_wait(item);
	}
		break;
	case 0x5F: // private_data_specifier_descriptor
	{
		private_data_specifier prov;
		unsigned int tmp = dvb->dvb_thread->read32();
		item->text = QString("PrivateDataSpecifier: %1 - %2").arg(tohex(tmp,8)).arg(prov.whatis(tmp));
		tree_create_wait(item);
	}
		break;
	case 0x62: // frequency_list_descriptor
	{
		coding_type ct;
		item->text = QString("Coding Type: %1").arg(ct.text.at(dvb->dvb_thread->read8(0x3)));
		tree_create_wait(item);
		while(dvb->dvb_thread->index < desc_end) {
			item->text = QString("Centre Frequency: %1").arg(dvb->dvb_thread->read32());
			tree_create_wait(item);
		}
	}
		break;
	case 0x66: // data_broadcast_id_descriptor
	{
		data_broadcast_id dbid;
		unsigned int tmp = dvb->dvb_thread->read16();
		item->text = QString("Data Broadcast ID: %1 - %2").arg(tmp).arg(dbid.whatis(tmp));
		tree_create_wait(item);
	}
		break;
	case 0x69: // PDC_descriptor
	{
		unsigned int tmp = dvb->dvb_thread->read24();
		item->text = QString("Programme Identification Label: Date:%2-%1 Time:%3:%4 ").arg(dvb->dvb_thread->maskbits(tmp, 0xF8000)).arg(dvb->dvb_thread->maskbits(tmp, 0x7800)).arg(dvb->dvb_thread->maskbits(tmp, 0x7C0)).arg(dvb->dvb_thread->maskbits(tmp, 0x3F));
		tree_create_wait(item);
	}
		break;
	case 0x6a: // AC-3_descriptor
	{
		unsigned int tmp = dvb->dvb_thread->read8();
		if (dvb->dvb_thread->maskbits(tmp, 0x80)) {
			item->text = QString("Component Type: %1").arg(dvb->dvb_thread->read8());
			tree_create_wait(item);
		}
		if (dvb->dvb_thread->maskbits(tmp, 0x40)) {
			item->text = QString("BSID: %1").arg(dvb->dvb_thread->read8());
			tree_create_wait(item);
		}
		if (dvb->dvb_thread->maskbits(tmp, 0x20)) {
			item->text = QString("MANID: %1").arg(dvb->dvb_thread->read8());
			tree_create_wait(item);
		}
		if (dvb->dvb_thread->maskbits(tmp, 0x10)) {
			item->text = QString("ASVC: %1").arg(dvb->dvb_thread->read8());
			tree_create_wait(item);
		}
	}
		break;
	case 0x81: // AC-3_audio_stream_descriptor
	{
		ac3_desc ac3;
		unsigned int tmp;
		tmp = dvb->dvb_thread->read8();
		item->text = QString("Sample Rate: %1").arg(ac3.sample_rate_code.at(dvb->dvb_thread->maskbits(tmp,0xE0)));
		tree_create_wait(item);
		item->text = QString("BSID: %1").arg(dvb->dvb_thread->maskbits(tmp,0x1F));
		tree_create_wait(item);
		tmp = dvb->dvb_thread->read8();
		item->text = QString("Bit Rate: %1 %2").arg(ac3.bit_rate_code.at(dvb->dvb_thread->maskbits(tmp,0x7C))).arg(dvb->dvb_thread->maskbits(tmp,0x80) ? "Upper Limit" : "Exact Rate");
		tree_create_wait(item);
		item->text = QString("Surround Mode: %1").arg(ac3.dsurmod.at(dvb->dvb_thread->maskbits(tmp,0xC0)));
		tree_create_wait(item);
		tmp = dvb->dvb_thread->read8();
		unsigned int bsmod = dvb->dvb_thread->maskbits(tmp,0x38);
		item->text = QString("Bit Stream Mode: %1").arg(ac3.bsmode.at(bsmod));
		tree_create_wait(item);
		unsigned int num_channels = dvb->dvb_thread->maskbits(tmp,0x1E);
		item->text = QString("Num Channels: %1").arg(ac3.num_channels.at(num_channels));
		tree_create_wait(item);
		item->text = QString("Full Service: %1").arg(dvb->dvb_thread->maskbits(tmp,0x01) ? "Yes" : "No");
		tree_create_wait(item);
	}
		break;
	case 0x82: // frame_rate_descriptor
	{
		unsigned int tmp;
		tmp = dvb->dvb_thread->read8();
		item->text = QString("Multiple Frame Rates: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x80) ? "Yes" : "No");
		tree_create_wait(item);
		frame_rate_code frc;
		item->text = QString("Frame Rate Code: %1").arg(frc.text.at(dvb->dvb_thread->maskbits(tmp, 0x78)));
		tree_create_wait(item);
	}
		break;
	case 0x83: // logical_channel_descriptor
	{
		while (dvb->dvb_thread->index < desc_end) {
			item->text = QString("Service ID: %1").arg(dvb->dvb_thread->read16());
			tree_create_wait(item);
			unsigned int tmp = dvb->dvb_thread->read16();
			item->text = QString("Visible: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x8000) ? "true" : "false");
			tree_create_wait(item);
			item->text = QString("Logical Channel Number: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x3FFF));
			tree_create_wait(item);
		}
	}
		break;
	case 0x86: // caption_service_descriptor
	{
		unsigned int num_services = dvb->dvb_thread->read8(0x1F);
		for (unsigned int i = 0; i < num_services; i++) {
			item->text = QString("Language: %1").arg(dvb->dvb_thread->readstr(3));
			tree_create_wait(item);
			unsigned int tmp;
			tmp = dvb->dvb_thread->read8();
			item->text = QString("Digital CC: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x80) ? "true" : "false");
			tree_create_wait(item);
			if (dvb->dvb_thread->maskbits(tmp, 0x80)) {
				item->text = QString("Line 21 field: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x01) ? "true" : "false");
				tree_create_wait(item);
			} else {
				item->text = QString("Caption Service Number: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x1F));
			}
			tmp = dvb->dvb_thread->read16();
			item->text = QString("Easy Reader: %1").arg(dvb->dvb_thread->maskbits(tmp,0x8000) ? "true" : "false");
			tree_create_wait(item);
			item->text = QString("Wide Aspect Ration: %1").arg(dvb->dvb_thread->maskbits(tmp,0x4000) ? "true" : "false");
			tree_create_wait(item);
		}
	}
		break;
	case 0x87: // content_advisory_descriptor
	{
		rating_region rr;
		unsigned int rating_region_count = dvb->dvb_thread->read8(0x3F);
		for (unsigned int i = 0; i < rating_region_count; i++) {
			item->text = QString("Rating Region: %1").arg(rr.name.at(dvb->dvb_thread->read8()));
			tree_create_wait(item);
			unsigned int rated_dimensions = dvb->dvb_thread->read8();
			for (unsigned int a = 0; a < rated_dimensions; a++) {
				item->text = QString("Rating Dimension J: %1").arg(dvb->dvb_thread->read8());
				tree_create_wait(item);
				item->text = QString("Rating Value: %1").arg(dvb->dvb_thread->read8(0x0F));
				tree_create_wait(item);
			}
			if (dvb->dvb_thread->read8() > 0) {
				parse_etm(item, "Rating Description");
			}
			item->restore();
		}
	}
		break;
	case 0x8A: // cue_identifier_descriptor
	{
		cue_stream cue;
		item->text = QString("CUE Stream Type: %1").arg(cue.whatis(dvb->dvb_thread->read8()));
		tree_create_wait(item);
	}
		break;
	case 0xA0: // extended_channel_name_descriptor
		while (dvb->dvb_thread->index < desc_end) {
			int number_strings = dvb->dvb_thread->read8();
			for (int i = 0; i < number_strings; i++) {
				dvb->dvb_thread->index += 3;
				int number_segments = dvb->dvb_thread->read8();
				for (int a = 0; a < number_segments; a++) {
					dvb->dvb_thread->index += 2;
					unsigned int num_bytes = dvb->dvb_thread->read8();
					if (num_bytes > 0) {
						item->text = QString("Long Channel Name: %1").arg(dvb->dvb_thread->readstr(num_bytes));
						tree_create_wait(item);
					}
				}
			}
		}
		break;
	case 0xA1: // service_location_descriptor
	{
		item->text	= QString("PCR: %1").arg(tohex(dvb->dvb_thread->read16(0x1FFF),4));
		tree_create_wait(item);
		unsigned int number_elements = dvb->dvb_thread->read8();
		for (unsigned int i = 0; i < number_elements; i++) {
			unsigned int stream_type = dvb->dvb_thread->read8();
			item->text	= QString("Stream PID: %1, Type: %2 - %3").arg(tohex(dvb->dvb_thread->read16(0x1FFF),4)).arg(tohex(stream_type,2)).arg(dvbnames.stream_type[stream_type]);
			tree_create_wait(item);
			item->text = QString("Language: %1").arg(dvb->dvb_thread->readstr(3));
			tree_create_wait(item);
		}
	}
		break;
	case 0xA2: // time_shifted_service_descriptor
	{
		unsigned int num_services = dvb->dvb_thread->read8(0x1F);
		for (unsigned int i = 0; i < num_services; i++) {
			item->text = QString("Time Shift: %1 min").arg(dvb->dvb_thread->read16(0x3FF));
			tree_create_wait(item);
			item->text = QString("Channel: %1-%2").arg(dvb->dvb_thread->read16(0x3FF)).arg(dvb->dvb_thread->read16(0x3FF));
			tree_create_wait(item);
		}
	}
		break;
	case 0xA3: // component_name_descriptor
		parse_etm(item, "Component Name");
		break;
	case 0xAA: // rc_descriptor
	{
		while (dvb->dvb_thread->index < desc_end) {
			item->text = QString("Redistribution Control Information: %1").arg(dvb->dvb_thread->read8());
			tree_create_wait(item);
		}
	}
		break;
	case 0xAD: // ATSC_private_information_descriptor
	{
		item->text = QString("Registration Descriptor: %1").arg(dvb->dvb_thread->read32());
		tree_create_wait(item);
	}
		break;
	case 0xB7: // Frame_payload_format_descriptor
	{
		unsigned int count = dvb->dvb_thread->read8();
		for (unsigned int i = 0; i < count; i++) {
			transmission_context_id tc_id;
			item->text = QString("Transmission Context ID: %1").arg(tc_id.whatis(dvb->dvb_thread->read8()));
			tree_create_wait(item);

			unsigned int tmp;
			tmp = dvb->dvb_thread->read8();
			item->text = QString("Allow ptype omission: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x10) ? "true" : "false");
			tree_create_wait(item);
			item->text = QString("Use compressed ptype: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x08) ? "true" : "false");
			tree_create_wait(item);
			item->text = QString("Allow alpdu crc: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x04) ? "true" : "false");
			tree_create_wait(item);
			item->text = QString("Allow alpdu sequence number: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x02) ? "true" : "false");
			tree_create_wait(item);
			item->text = QString("Use explicit payload header map: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x01) ? "true" : "false");
			tree_create_wait(item);

			item->text = QString("Implicit Protocol Type: %1").arg(dvb->dvb_thread->read8());
			tree_create_wait(item);

			tmp = dvb->dvb_thread->read8();
			item->text = QString("Implicit ppdu label size: %1").arg(dvb->dvb_thread->maskbits(tmp, 0xf0));
			tree_create_wait(item);
			item->text = QString("Implicit payload label size: %1").arg(dvb->dvb_thread->maskbits(tmp, 0x0f));
			tree_create_wait(item);

			tmp = dvb->dvb_thread->read8();
			item->text = QString("Type 0 alpdu label size: %1").arg(dvb->dvb_thread->read8(0x0f));
			tree_create_wait(item);
		}
	}
		break;
	default:
		qDebug() << Q_FUNC_INFO << "Unknown Descriptor";
		dump_desc();
		break;
	}

	item->parent		= orig.parent;
	item->current		= orig.current;
	item->return_parent	= orig.return_parent;
	dvb->dvb_thread->index = desc_end;
}

void tuning_tab_thread::dump_desc()
{
	unsigned int o_index = dvb->dvb_thread->index;
	unsigned int desc_tag = dvb->dvb_thread->read8();
	unsigned int desc_len = dvb->dvb_thread->read8();

	qDebug() << Q_FUNC_INFO << "Descriptor Tag" << tohex(desc_tag,2) << ":" << desc_len << "bytes";
	qDebug() << "ASCII:";
	qDebug() << dvb->dvb_thread->buffer.mid(dvb->dvb_thread->index, desc_len);
	qDebug() << "HEX:";
	QString output;
	for (unsigned int i = 0; i < desc_len; i++) {
		if (i % 8 == 0 && !output.isEmpty()) {
			qDebug() << output;
			output.clear();
		}
		output.append(tohex(dvb->dvb_thread->read8(),2));
		output.append(" ");
	}
	qDebug() << output;

	dvb->dvb_thread->index = o_index;
}

void tuning_tab_thread::parse_psip_tvct()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item	= new tree_item;
	item->pid	= pid;
	item->text	= QString("PSIP pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);

	item->text = QString("TVCT - Terrestrial Virtual Channel Table");
	tree_create_wait(item);
	item->pid = 0xFFFF;
	item->return_parent = false;

	dvb->dvb_thread->index += 2;

	item->text = QString("Transport Stream ID: %1").arg(dvb->dvb_thread->read16());
	tree_create_wait(item);

	dvb->dvb_thread->index += 4;

	unsigned int num_channels_in_section = dvb->dvb_thread->read8();
	for (unsigned int i = 0; i < num_channels_in_section; i++) {
		item->text = QString("Short Name: %1").arg(dvb->dvb_thread->readstr16(7));
		tree_create_wait(item);

		unsigned int channel = dvb->dvb_thread->read24();
		item->text = QString("Channel Number: %1-%2").arg(dvb->dvb_thread->maskbits(channel, 0xFFC00)).arg(dvb->dvb_thread->maskbits(channel, 0x3FF));
		tree_create_wait(item);
		atsc_modulation am;
		item->text = QString("Modulation: %1").arg(am.whatis(dvb->dvb_thread->read8()));
		tree_create_wait(item);
		unsigned int freq = dvb->dvb_thread->read32();
		if (freq > 0) {
			item->text = QString("Carrier Frequency (deprecated): %1mhz").arg(freq/1000000.0);
			tree_create_wait(item);
		}
		item->text = QString("Channel TSID: %1").arg(dvb->dvb_thread->read16());
		tree_create_wait(item);
		item->text = QString("Program Number: %1").arg(dvb->dvb_thread->read16());
		tree_create_wait(item);
		etm_location etm;
		unsigned int tmp = dvb->dvb_thread->read16();
		item->text = QString("ETM Location: %1").arg(etm.text.at(dvb->dvb_thread->maskbits(tmp,0xC000)));
		tree_create_wait(item);
		item->text = QString("Access Controlled: %1").arg(dvb->dvb_thread->maskbits(tmp,0x2000) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Hidden: %1").arg(dvb->dvb_thread->maskbits(tmp,0x1000) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Hide Guide: %1").arg(dvb->dvb_thread->maskbits(tmp,0x0200) ? "true" : "false");
		tree_create_wait(item);
		atsc_service_type ast;
		item->text = QString("Service Type: %1").arg(ast.whatis(dvb->dvb_thread->maskbits(tmp,0x3F)));
		tree_create_wait(item);
		item->text = QString("Source ID: %1").arg(dvb->dvb_thread->read16());
		tree_create_wait(item);

		unsigned int descriptors_length = dvb->dvb_thread->index + dvb->dvb_thread->read16(0x3FF);
		while (dvb->dvb_thread->index < descriptors_length) {
			parse_descriptor(item);
		}
	}
	unsigned int additional_descriptors_length = dvb->dvb_thread->index + dvb->dvb_thread->read16(0x3FF);
	while (dvb->dvb_thread->index < additional_descriptors_length) {
		parse_descriptor(item);
	}
}

void tuning_tab_thread::parse_psip_cvct()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item	= new tree_item;
	item->pid	= pid;
	item->text	= QString("PSIP pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);

	item->text = QString("CVCT - Cable Virtual Channel Table");
	tree_create_wait(item);
	item->pid = 0xFFFF;
	item->return_parent = false;

	dvb->dvb_thread->index += 2;

	item->text = QString("Transport Stream ID: %1").arg(dvb->dvb_thread->read16());
	tree_create_wait(item);

	dvb->dvb_thread->index += 4;

	unsigned int num_channels_in_section = dvb->dvb_thread->read8();
	for (unsigned int i = 0; i < num_channels_in_section; i++) {
		item->text = QString("Short Name: %1").arg(dvb->dvb_thread->readstr16(7));
		tree_create_wait(item);

		unsigned int channel = dvb->dvb_thread->read24();
		item->text = QString("Channel Number: %1-%2").arg(dvb->dvb_thread->maskbits(channel, 0xFFC00)).arg(dvb->dvb_thread->maskbits(channel, 0x3FF));
		tree_create_wait(item);
		atsc_modulation am;
		item->text = QString("Modulation: %1").arg(am.whatis(dvb->dvb_thread->read8()));
		tree_create_wait(item);
		unsigned int freq = dvb->dvb_thread->read32();
		if (freq > 0) {
			item->text = QString("Carrier Frequency (deprecated): %1mhz").arg(freq/1000000.0);
			tree_create_wait(item);
		}
		item->text = QString("Channel TSID: %1").arg(dvb->dvb_thread->read16());
		tree_create_wait(item);
		item->text = QString("Program Number: %1").arg(dvb->dvb_thread->read16());
		tree_create_wait(item);
		etm_location etm;
		unsigned int tmp = dvb->dvb_thread->read16();
		item->text = QString("ETM Location: %1").arg(etm.text.at(dvb->dvb_thread->maskbits(tmp,0xC000)));
		tree_create_wait(item);
		item->text = QString("Access Controlled: %1").arg(dvb->dvb_thread->maskbits(tmp,0x2000) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Hidden: %1").arg(dvb->dvb_thread->maskbits(tmp,0x1000) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Path Select: %1").arg(dvb->dvb_thread->maskbits(tmp,0x800) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Out Of Band: %1").arg(dvb->dvb_thread->maskbits(tmp,0x400) ? "true" : "false");
		tree_create_wait(item);
		item->text = QString("Hide Guide: %1").arg(dvb->dvb_thread->maskbits(tmp,0x0200) ? "true" : "false");
		tree_create_wait(item);
		atsc_service_type ast;
		item->text = QString("Service Type: %1").arg(ast.whatis(dvb->dvb_thread->maskbits(tmp,0x3F)));
		tree_create_wait(item);
		item->text = QString("Source ID: %1").arg(dvb->dvb_thread->read16());
		tree_create_wait(item);

		unsigned int descriptors_length = dvb->dvb_thread->index + dvb->dvb_thread->read16(0x3FF);
		while (dvb->dvb_thread->index < descriptors_length) {
			parse_descriptor(item);
		}
	}
	unsigned int additional_descriptors_length = dvb->dvb_thread->index + dvb->dvb_thread->read16(0x3FF);
	while (dvb->dvb_thread->index < additional_descriptors_length) {
		parse_descriptor(item);
	}
}

void tuning_tab_thread::parse_psip_eit()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item	= new tree_item;
	item->pid	= pid;
	item->text	= QString("PSIP pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);

	item->text = QString("EIT - Event Information Table");
	tree_create_wait(item);
	item->pid = 0xFFFF;

	dvb->dvb_thread->index += 2;

	item->text = QString("SourceID: %1").arg(tohex(dvb->dvb_thread->read16(),2));
	tree_create_wait(item);

	dvb->dvb_thread->index += 4;

	unsigned int num_channels_in_section = dvb->dvb_thread->read8();
	for (unsigned int i = 0; i < num_channels_in_section; i++) {
		tree_item orig = *item;

		item->return_parent = true;
		item->text = QString("Event ID: %1").arg(tohex(dvb->dvb_thread->read16(0x3FFF),4));
		tree_create_wait(item);
		item->return_parent = false;

		item->text = QString("Start Time: %1").arg(QDateTime::fromTime_t(dvb->dvb_thread->read32() + 315964800).toString());
		tree_create_wait(item);

		__u32 dtime = dvb->dvb_thread->read24(0x0FFF);
		unsigned int h = dtime/60/60;
		unsigned int m = dtime/60 - (h*60);
		unsigned int s = dtime - (h*60*60) - (m*60);
		item->text = QString("Duration: %1").arg(QTime(h, m, s).toString());
		tree_create_wait(item);

		dvb->dvb_thread->index++;

		parse_etm(item, "Event Name");

		unsigned int descriptors_length = dvb->dvb_thread->index + dvb->dvb_thread->read16(0xFFF);
		while (dvb->dvb_thread->index < descriptors_length) {
			parse_descriptor(item);
		}

		*item = orig;
	}
}

void tuning_tab_thread::parse_psip_stt()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->text	= QString("PSIP pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);

	item->text	= QString("STT - System Time Table");
	item->expanded	= false;
	tree_create_wait(item);
	item->pid	= 0xFFFF;
	item->return_parent = false;

	unsigned int section_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index - 4;
	dvb->dvb_thread->index += 6;

	item->text = QString("System Time: %1").arg(QDateTime::fromTime_t(dvb->dvb_thread->read32() + 315964800).toString());
	tree_create_wait(item);
	item->text = QString("GPS_UTC_offset: %1").arg(dvb->dvb_thread->read8());
	tree_create_wait(item);
	unsigned int dst = dvb->dvb_thread->read16();
	item->text = QString("DS status: %1 ").arg(dvb->dvb_thread->maskbits(dst,0x8000) ? "true" : "false") +
			QString("DS day of month: %1 ").arg(dvb->dvb_thread->maskbits(dst,0x1f00)) +
			QString("DS hour: %1").arg(dvb->dvb_thread->maskbits(dst,0xff));
	tree_create_wait(item);
	while (dvb->dvb_thread->index < section_length) {
		parse_descriptor(item);
	}
}

void tuning_tab_thread::parse_psip_mgt()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->text	= QString("PSIP pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);

	item->text = QString("MGT - Master Guide Table");
	tree_create_wait(item);
	item->pid	= 0xFFFF;
	item->return_parent = false;

	dvb->dvb_thread->index += 4;

	item->text = QString("Version Number: %1 ").arg(tohex(dvb->dvb_thread->read8(0x3E),2));
	tree_create_wait(item);

	dvb->dvb_thread->index += 2;

	item->text = QString("Protocol Version: %1 ").arg(tohex(dvb->dvb_thread->read8(),2));
	tree_create_wait(item);

	unsigned int num_tables = dvb->dvb_thread->read16();
	for (unsigned int i = 0; i < num_tables; i++) {
		mgt_table mgt;
		unsigned int table_type	= dvb->dvb_thread->read16();
		unsigned int table_pid	= dvb->dvb_thread->read16(0x1FFF);

		dvb->dvb_thread->index += 5;

		item->text = QString("Table Type: %1 / PID: %2 - %3").arg(tohex(table_type,4)).arg(tohex(table_pid,4)).arg(mgt.whatis(table_type));
		tree_create_wait(item);

		if (table_type >= 0x0100 && table_type <= 0x17F) {
			filter_pids(table_pid, {0xCB}, {0xFF}, 1000);
		} else if ((table_type >= 0x0200 && table_type <= 0x27F) || table_type == 0x0004) {
			filter_pids(table_pid, {0xCC}, {0xFF}, 1000);
		} else if ((table_type >= 0x0301 && table_type <= 0x3FF)) {
			filter_pids(table_pid, {0xCA}, {0xFF}, 1000);
		} else if (table_type == 0x0000) {
			filter_pids(table_pid, {0xC8}, {0xFF}, 1000);
		} else if (table_type == 0x0002) {
			filter_pids(table_pid, {0xC9}, {0xFF}, 1000);
		} else {
			qDebug() << Q_FUNC_INFO << "Unknown PID:" << tohex(table_pid,4) << "& table_type:" << tohex(table_type,4);
		}

		unsigned int desc_len = dvb->dvb_thread->read16(0x0FFF);
		for (unsigned int a = 0; a < desc_len; a++) {
			parse_descriptor(item);
		}
	}
	unsigned int desc_len = dvb->dvb_thread->read16(0x0FFF);
	for (unsigned int a = 0; a < desc_len; a++) {
		parse_descriptor(item);
	}
}

void tuning_tab_thread::parse_psip_ett()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->text	= QString("PSIP pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);

	item->text	= QString("ETT - Extended Text Table");
	tree_create_wait(item);
	item->pid	= 0xFFFF;

	dvb->dvb_thread->index += 8;

	item->text = QString("ETM ID : %2").arg(tohex(dvb->dvb_thread->read32(),8));
	tree_create_wait(item);
	item->return_parent = false;

	parse_etm(item, "Text");
}

void tuning_tab_thread::parse_psip_rrt()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->text	= QString("PSIP pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);

	item->text = QString("RRT - Rating Region Table");
	tree_create_wait(item);
	item->pid	= 0xFFFF;
	item->return_parent = false;

	dvb->dvb_thread->index += 9;

	parse_etm(item, "Rating Region Name");

	int values_parent;
	unsigned int dimensions_defined = dvb->dvb_thread->read8();
	for (unsigned int i = 0; i < dimensions_defined; i++) {
		int dimensions_parent = item->parent;

		dvb->dvb_thread->index++;
		parse_etm(item, "Dimension Name");

		unsigned int values_defined = dvb->dvb_thread->read8(0x0F);
		for (unsigned int j = 0; j < values_defined; j++) {
			values_parent = item->parent;

			item->return_parent	= true;
			item->text			= QString("Rating");
			tree_create_wait(item);
			item->return_parent	= false;

			dvb->dvb_thread->index++;
			parse_etm(item, "Abbreviated Rating Name");

			dvb->dvb_thread->index++;
			parse_etm(item, "Rating Name");

			item->parent = values_parent;
		}

		item->parent = dimensions_parent;
	}

	unsigned int descriptors_loop_length = dvb->dvb_thread->read16(0x03FF) + dvb->dvb_thread->index;
	while (dvb->dvb_thread->index < descriptors_loop_length) {
		parse_descriptor(item);
	}
}

void tuning_tab_thread::parse_eit()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->text	= QString("EIT pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);
	item->pid	= 0xFFFF;
	item->return_parent = false;

	unsigned int section_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index - 4;

	item->text = QString("Program Number: %1").arg(dvb->dvb_thread->read16());
	tree_create_wait(item);
	item->return_parent = false;

	dvb->dvb_thread->index += 9;

	while (dvb->dvb_thread->index < section_length) {
		item->text = QString("Event ID: %1").arg(dvb->dvb_thread->read16());
		tree_create_wait(item);

		__u16 t1 = dvb->dvb_thread->read16();
		__u8  t2 = dvb->dvb_thread->read8();
		__u8  t3 = dvb->dvb_thread->read8();
		__u8  t4 = dvb->dvb_thread->read8();
		__u8  t5 = dvb->dvb_thread->read8();
		__u8  t6 = dvb->dvb_thread->read8();
		__u8  t7 = dvb->dvb_thread->read8();
		item->text = QString("Start Date/Time: %1 %2:%3:%4").arg(QDate::fromJulianDay(t1 + 2400000.5).toString()).arg(dtag_convert(t2), 2, 10, QChar('0')).arg(dtag_convert(t3), 2, 10, QChar('0')).arg(dtag_convert(t4), 2, 10, QChar('0'));
		tree_create_wait(item);
		item->text = QString("Duration: %1:%2:%3").arg(dtag_convert(t5), 2, 10, QChar('0')).arg(dtag_convert(t6), 2, 10, QChar('0')).arg(dtag_convert(t7), 2, 10, QChar('0'));
		tree_create_wait(item);

		unsigned int descriptors_loop_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index;
		while (dvb->dvb_thread->index < descriptors_loop_length) {
			parse_descriptor(item);
		}
	}
}

void tuning_tab_thread::parse_sdt()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item	= new tree_item;
	item->pid	= pid;
	item->text	= QString("SDT pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);
	item->pid	= 0xFFFF;
	item->return_parent = false;

	mysdt.sid.clear();
	mysdt.sname.clear();
	mysdt.pname.clear();

	unsigned int section_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index - 4;

	dvb->dvb_thread->index += 8;

	while (dvb->dvb_thread->index < section_length) {
		mysdt.sid.append(dvb->dvb_thread->read16());

		item->text = QString("Service ID: %1").arg(mysdt.sid.last());
		tree_create_wait(item);

		dvb->dvb_thread->index += 1;

		unsigned int descriptors_loop_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index;
		while (dvb->dvb_thread->index < descriptors_loop_length) {
			parse_descriptor(item);
		}
	}
}

void tuning_tab_thread::parse_pat()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->text	= QString("PAT pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);
	item->pid	= 0xFFFF;

	mypat.number.clear();
	mypat.pid.clear();

	unsigned int section_length = dvb->dvb_thread->read16(0x0FFF) - 4;

	dvb->dvb_thread->index += 5;

	while (dvb->dvb_thread->index < section_length) {
		tree_item orig = *item;

		mypat.number.append(dvb->dvb_thread->read16());
		mypat.pid.append(dvb->dvb_thread->read16(0x1FFF));
		item->text = QString("Program Number: %1").arg(mypat.number.last());
		tree_create_wait(item);
		item->text = QString("Program PID: %1").arg(tohex(mypat.pid.last(),4));
		tree_create_wait(item);

		*item = orig;
		filter_pids(mypat.pid.last(), {0xC1}, {0xFF}, 2000);
	}
}

void tuning_tab_thread::parse_cat()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->color	= QColor(Qt::red);
	item->text	= QString("CAT pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);
	item->pid	= 0xFFFF;
	item->return_parent = false;

	unsigned int section_length = dvb->dvb_thread->read16(0x0FFF) - 4;

	dvb->dvb_thread->index += 5;

	while (dvb->dvb_thread->index < section_length) {
		parse_descriptor(item);
	}
}

void tuning_tab_thread::parse_nit()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->text	= QString("NIT pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);
	item->pid	= 0xFFFF;

	unsigned int section_length = dvb->dvb_thread->read16(0x0FFF);

	item->text = QString("Network ID: %1").arg(tohex(dvb->dvb_thread->read16(),4));
	tree_create_wait(item);
	item->return_parent = false;

	dvb->dvb_thread->index += 3;

	unsigned int network_desc_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index;
	while (dvb->dvb_thread->index < network_desc_length && dvb->dvb_thread->index < section_length) {
		parse_descriptor(item);
	}

	unsigned int transport_stream_loop_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index;
	while (dvb->dvb_thread->index < transport_stream_loop_length) {
		item->text = QString("Transport Stream ID: %1, Original Network ID: %2").arg(tohex(dvb->dvb_thread->read16(),4)).arg(tohex(dvb->dvb_thread->read16(),4));
		tree_create_wait(item);

		unsigned int transport_desc_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index;
		while (dvb->dvb_thread->index < transport_desc_length && dvb->dvb_thread->index < section_length) {
			parse_descriptor(item);
		}
	}
}

void tuning_tab_thread::parse_pmt()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;

	unsigned int section_length = dvb->dvb_thread->read16(0x0FFF) - 4;

	item->text = QString("PMT PID: %1 - Program: %2").arg(tohex(pid,4)).arg(dvb->dvb_thread->read16());
	tree_create_wait(item);
	item->return_parent = false;

	dvb->dvb_thread->index += 3;

	item->pid	= dvb->dvb_thread->read16(0x1FFF);
	item->text	= QString("PCR: %1").arg(tohex(item->pid,4));
	tree_create_wait(item);
	item->pid	= 0xFFFF;

	unsigned int program_info_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index;
	while (dvb->dvb_thread->index < program_info_length) {
		parse_descriptor(item);
	}

	while (dvb->dvb_thread->index < section_length) {
		unsigned int desc_type	= dvb->dvb_thread->read8();
		unsigned int desc_pid	= dvb->dvb_thread->read16(0x1FFF);

		item->pid	= desc_pid;
		item->text	= QString("Stream PID: %1, Type: %2 - %3").arg(tohex(desc_pid,4)).arg(tohex(desc_type,2)).arg(dvbnames.stream_type[desc_type]);
		tree_create_wait(item);
		item->pid	= 0xFFFF;

		unsigned int ES_info_length = dvb->dvb_thread->read16(0x0FFF) + dvb->dvb_thread->index;
		while (dvb->dvb_thread->index < ES_info_length) {
			parse_descriptor(item);
		}
	}
}

void tuning_tab_thread::parse_tdt()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;
	item->expanded	= false;
	item->text	= QString("TDT pid: %1").arg(tohex(pid,4));
	tree_create_wait(item);
	item->pid	= 0xFFFF;
	item->return_parent = false;

	dvb->dvb_thread->index += 2;

	__u16 t1 = dvb->dvb_thread->read16();
	__u8  t2 = dvb->dvb_thread->read8();
	__u8  t3 = dvb->dvb_thread->read8();
	__u8  t4 = dvb->dvb_thread->read8();

	// If you change this text, change it in tuning::tree_create_child() as well
	item->text = QString("UTC Date/Time: %1 %2:%3:%4").arg(QDate::fromJulianDay(t1 + 2400000.5).toString()).arg(dtag_convert(t2),2).arg(dtag_convert(t3),2).arg(dtag_convert(t4),2);
	tree_create_wait(item);
}

void tuning_tab_thread::parse_dcii_sdt()
{
	if (dvb->dvb_thread->packet_processed.contains(dvb->dvb_thread->buffer)) {
		return;
	}
	dvb->dvb_thread->packet_processed.append(dvb->dvb_thread->buffer);

	unsigned int pid = dvb->dvb_thread->dvbdata.first().pid;
	tree_item *item = new tree_item;
	item->pid	= pid;

	dvb->dvb_thread->index = 15;
	unsigned int service_name_length = dvb->dvb_thread->read8();
	item->text = QString("DC2 PID: %1 - %2").arg(tohex(pid,4)).arg(dvb->dvb_thread->readstr(service_name_length));
	tree_create_wait(item);
	item->pid = 0xFFFF;
	item->return_parent = false;
}


void tuning_tab_thread::filter_pids(unsigned int pid, QVector<unsigned int> table, QVector<unsigned int> mask, unsigned int timeout)
{
	if (dvb->dvb_thread->pids_rate.at(pid) > 0) {
		dvb_pids tmp;
		tmp.pid		= pid;
		tmp.tbl		= table;
		tmp.msk		= mask;
		tmp.timeout	= timeout;
		if (dvb_pids_containts(fpids, tmp)) {
			return;
		}
		fpids.append(tmp);
	}
}

void tuning_tab_thread::parsetp()
{
	int stt = 0;

	filter_pids(0x0001, {0x01}, {0xFF}, 2000);
	filter_pids(0x0011, {0x42}, {0xFF}, 2000);
	filter_pids(0x1FFB, {0xC7}, {0xFF}, 2000);
	filter_pids(0x1FFB, {0xC8}, {0xFF}, 2000);
	filter_pids(0x1FFB, {0xCB}, {0xFF}, 2000);
	filter_pids(0x1FFB, {0xCD}, {0xFF}, 2000);
	filter_pids(0x1FFB, {0xCA}, {0xFF}, 2000);
	filter_pids(0x0012, {0x4E}, {0xFF}, 2000);
	filter_pids(0x0010, {0x40}, {0xFF}, 2000);
	filter_pids(0x0000, {0x00}, {0xFF}, 2000);
	filter_pids(0x0014, {0x70}, {0xFF}, 2000);
	for (int i = 0; i < mypat.pid.size(); i++) {
		filter_pids(mypat.pid.at(i),
		{0x02, (mypat.number.at(i) >> 8) & 0xFF, mypat.number.at(i) & 0xFF},
		{0xFF, 0xFF, 0xFF},
			    2000);
	}
	dvb->dvb_thread->demux_packets(fpids);

	while (!dvb->dvb_thread->dvbdata.isEmpty() && loop) { // checking that loop is true isnt required but this is a big function, if we wish to quit, this really helps speed up the quit()
		dvb->dvb_thread->buffer = dvb->dvb_thread->dvbdata.first().buffer;
		dvb->dvb_thread->index = 0;
		switch (dvb->dvb_thread->read8()) { // Table ID
		case 0x4E:
			parse_eit();
			break;
		case 0x42:
			parse_sdt();
			break;
		case 0xCB:
			parse_psip_eit();
			break;
		case 0xC7:
			parse_psip_mgt();
			break;
		case 0xC8:
			parse_psip_tvct();
			break;
		case 0xC9:
			parse_psip_cvct();
			break;
		case 0xCA:
			parse_psip_rrt();
			break;
		case 0xCC:
			parse_psip_ett();
			break;
		case 0xCD:
			if(stt == 0) {
				parse_psip_stt();
				stt = stt +1; // parse stt once
			}
			break;
		case 0x00:
			parse_pat();
			break;
		case 0x01:
			parse_cat();
			break;
		case 0x40:
			parse_nit();
			break;
		case 0x02:
			parse_pmt();
			break;
		case 0x70:
			parse_tdt();
			break;
		case 0xC1:
			parse_dcii_sdt();
			break;
		default:
			qDebug() << Q_FUNC_INFO << "Unknown TableID:" << hex << dvb->dvb_thread->dvbdata.first().pid << dvb->dvb_thread->dvbdata.first().table;
			break;
		}
		dvb->dvb_thread->dvbdata.removeFirst();
	}
}
