/*	
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dvbstream_class.h"

dvbstream_class::dvbstream_class(dvb_class *d)
{
	dvb = d;

	signal(SIGPIPE, SIG_IGN);
	server		 = NULL;
	socket		 = NULL;
	IP		 = QHostAddress::Null;
	port		 = 0;
}

dvbstream_class::~dvbstream_class()
{
	server_close();
	if (!socket.isNull()) {
		socket->deleteLater();
	}
	if (!server.isNull()) {
		server->deleteLater();
	}
}

void dvbstream_class::socket_new()
{
	if (!socket.isNull()) {
		socket->close();
		socket->waitForDisconnected(1000);
	}
	socket = server->nextPendingConnection();
	socket->setSocketOption(QAbstractSocket::LowDelayOption, 1);
	connect(socket, &QTcpSocket::disconnected, this, &dvbstream_class::socket_disconnect, Qt::DirectConnection);
	connect(socket, &QTcpSocket::readyRead, this, &dvbstream_class::read_data, Qt::DirectConnection);
	dvb->emit_update_status(QString("Streaming to %1").arg(socket->peerAddress().toString()), STATUS_NOEXP);
}

void dvbstream_class::process_data()
{
	if (data.last() != "\r\n") {
		return;
	}

	if (data.at(0).contains("GET / HTTP")) {
		QString ua = user_agent();
		// VLC/MPV/MPlayer clients
		if (ua.contains("VLC") || ua.contains("mpv") || ua.contains("MPlayer") || ua.contains("Enigma2")) {
			socket->write("HTTP/1.0 200 OK\r\n");
			socket->write("Content-type: application/octet-stream\r\n");
			socket->write("Cache-Control : no-cache\r\n");
			socket->write("\r\n");
			socket->waitForBytesWritten(2000);

			dvb->emit_update_status(QString("Streaming to %1 @ %2").arg(ua).arg(socket->peerAddress().toString()), STATUS_NOEXP);
			dvb->dvb_thread->demux_stream(true);
		}
	}
	data.clear();
}

QString dvbstream_class::user_agent()
{
	for (QString t : data) {
		if (t.section(" ", 0, 0) == "User-Agent:") {
			return t.section(" ", 1, 1);
		}
	}
	return "";
}

void dvbstream_class::read_data()
{
	while (socket->bytesAvailable()) {
		data.append(socket->readLine());
	}
	process_data();
}

void dvbstream_class::socket_disconnect()
{
	socket_close();
	dvb->emit_update_status(QString("Streaming on %1:%2").arg(IP.toString()).arg(port), STATUS_NOEXP);
}

void dvbstream_class::socket_close()
{
	if (!socket.isNull()) {
		dvb->dvb_thread->demux_stream(false);
		socket->close();
	}
}

void dvbstream_class::server_disconnect()
{
	server_close();
	dvb->emit_update_status("", STATUS_CLEAR);
	dvb->emit_update_status("Disconnected", 2);
}

void dvbstream_class::server_close()
{
	socket_close();
	if (!server.isNull()) {
		server->close();
	}

	IP	= QHostAddress::Null;
	port	= 0;
}

void dvbstream_class::server_new()
{
	QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
	for (QHostAddress t : ipAddressesList) {
		if (t != QHostAddress::LocalHost && t.toIPv4Address()) {
			IP = t;
			break;
		}
	}
	if (IP.isNull()) {
		IP = QHostAddress(QHostAddress::LocalHost);
	}
	port = 1230 + dvb->dvb_thread->adapter;
	server = new QTcpServer();
	if (!server->listen(QHostAddress::Any, port)) {
		qDebug() << "Server could not start";
		server_close();
		return;
	}
	connect(server, &QTcpServer::newConnection, this, &dvbstream_class::socket_new, Qt::DirectConnection);
	dvb->emit_update_status(QString("Streaming on %1:%2").arg(IP.toString()).arg(port), STATUS_NOEXP);
}

void dvbstream_class::stream(QByteArray data)
{
	if (!socket.isNull() && socket->state() == QAbstractSocket::ConnectedState) {
		qint64 len = socket->write(data);
		socket->waitForBytesWritten(2000);
		if (len != LIL_BUFSIZE) {
			qDebug() << "TCP write() issue:" << len << "of" << LIL_BUFSIZE;
		}
	}
}
