/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "blindscan_tab_worker.h"

blindscan_tab_worker::blindscan_tab_worker(dvb_class *d)
{
	dvb = d;
}

blindscan_tab_worker::~blindscan_tab_worker()
{
}

void blindscan_tab_worker::setup_freq()
{
	int f_start, f_stop;
	if (abs(dvb->dvb_thread->tune_ops.f_start - abs(dvb->dvb_thread->tune_ops.f_lof)) < abs(dvb->dvb_thread->tune_ops.f_stop - abs(dvb->dvb_thread->tune_ops.f_lof))) {
		f_start	= abs(dvb->dvb_thread->tune_ops.f_start - abs(dvb->dvb_thread->tune_ops.f_lof));
		f_stop	= abs(dvb->dvb_thread->tune_ops.f_stop - abs(dvb->dvb_thread->tune_ops.f_lof));
	} else {
		f_start	= abs(dvb->dvb_thread->tune_ops.f_stop - abs(dvb->dvb_thread->tune_ops.f_lof));
		f_stop	= abs(dvb->dvb_thread->tune_ops.f_start - abs(dvb->dvb_thread->tune_ops.f_lof));
	}

	if (f_start < dvb->dvb_thread->fmin/1000) {
		f_start = dvb->dvb_thread->fmin/1000;
	}
	if (f_start > dvb->dvb_thread->fmax/1000) {
		f_start = dvb->dvb_thread->fmax/1000;
	}
	if (f_stop < dvb->dvb_thread->fmin/1000) {
		f_stop = dvb->dvb_thread->fmin/1000;
	}
	if (f_stop > dvb->dvb_thread->fmax/1000) {
		f_stop = dvb->dvb_thread->fmax/1000;
	}
	if (!abs(f_stop-f_start)) {
		return;
	}

	freq_list myfreq;
	if (!isSatellite(dvb->dvb_thread->tp.system)) {
		if (isQAM(dvb->dvb_thread->tp.system)) {
			myfreq.qam();
		} else if (isATSC(dvb->dvb_thread->tp.system)) {
			myfreq.atsc();
		} else if (isDVBT(dvb->dvb_thread->tp.system)) {
			myfreq.dvbt();
		}

		if (dvb->dvb_thread->tp_try.size())
			return;

		tp_info tp;
		tp.system     = dvb->dvb_thread->tp.system;
		tp.modulation = dvb->dvb_thread->tp.modulation;

		dvb->dvb_thread->tp_try.clear();
		for (int freq : myfreq.freq) {
			if (freq >= f_start && freq <= f_stop) {
				tp.frequency = freq;
				dvb->dvb_thread->tp_try.append(tp);
			}
		}
	}
}
