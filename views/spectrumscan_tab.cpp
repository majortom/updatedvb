/*
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spectrumscan_tab.h"
#include "ui_spectrumscan_tab.h"

spectrumscan_tab::spectrumscan_tab(QWidget *parent, dvb_class *d) :
	QWidget(parent),
	ui(new Ui::spectrumscan_tab)
{
	ui->setupUi(this);
	ui->gridWidget_stats->hide();

	dvb = d;
	mc = dvb->mc;
	dvb->qwt_plot = ui->qwtPlot;
	worker = new spectrumscan_tab_worker(d);

	connect(worker->qwt_picker, SIGNAL(selected(QPointF)), this, SLOT(qwtPlot_selected(QPointF)));
	connect(worker->myspectrumscan_thread, &spectrumscan_thread::signaldraw, this, &spectrumscan_tab::qwt_draw);
	connect(worker->myspectrumscan_thread, &spectrumscan_thread::markers_draw, this, &spectrumscan_tab::markers_draw);

	reload_lnb();

	dvb->dvb_thread->frontend = ui->comboBox_frontend->currentData().toInt();
	dvb->dvb_thread->tune_ops = mc->tune_ops[ui->comboBox_lnb->currentData().toInt()];
	dvb->dvb_thread->getops();

	setup_tuning_options();

	noload = false;
	reload_settings();

	ui->comboBox_sr->clear();
	ui->comboBox_sr->addItem("1000");
	ui->comboBox_sr->addItem("7500");
	ui->comboBox_sr->addItem("15000");
	ui->comboBox_sr->addItem("20000");
	ui->comboBox_sr->addItem("30000");
}

spectrumscan_tab::~spectrumscan_tab()
{
	delete ui;
}

QPixmap spectrumscan_tab::grab_qwt_plot()
{
	return dvb->qwt_plot->grab();
}

void spectrumscan_tab::setup_tuning_options()
{
	dvb->dvb_thread->frontend = ui->comboBox_frontend->currentData().toInt();

	// If we cant open the adapter or its status us unavailible then hide everything
	if (!dvb->dvb_thread->openfd() || !(dvb->dvb_thread->status & TUNER_AVAIL)) {
		ui->gridWidget_blindscan->hide();
		ui->gridWidget_gotox->hide();
		ui->gridWidget_positioner->hide();
		ui->gridWidget_satellite->hide();
		ui->gridWidget_spectrumscan->hide();
		ui->gridWidget_usals->hide();
		ui->gridWidget_delsys->hide();
		ui->qwtPlot->setDisabled(true);
		return;
	}
	dvb->dvb_thread->getops();

	if (mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_diseqc_v12").toBool()) {
		ui->gridWidget_gotox->show();
	} else {
		ui->gridWidget_gotox->hide();
	}
	if (mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_diseqc_v13").toBool()) {
		ui->gridWidget_usals->show();
		ui->comboBox_usals->setCurrentText(mc->mysettings->value("adapter"+QString::number(dvb->dvb_thread->adapter)+"_usals_position").toString());
	} else {
		ui->gridWidget_usals->hide();
	}
	if (mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_diseqc_v12").toBool() || mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_diseqc_v13").toBool()) {
		ui->gridWidget_positioner->show();
	} else {
		ui->gridWidget_positioner->hide();
	}

	if (dvb->dvb_thread->extended_caps & FE_CAN_SPECTRUMSCAN) {
		ui->gridWidget_spectrumscan->show();
	} else {
		ui->gridWidget_spectrumscan->hide();
	}

	ui->comboBox_system->clear();
	if (dvb->dvb_thread->delsys.size()) {
		for (int t : dvb->dvb_thread->delsys) {
			ui->comboBox_system->addItem(dvbnames.system(t));
		}
	} else { // This should never happen, just in case some driver is seriously messed up
		ui->comboBox_system->addItem("DVB-S");
		ui->comboBox_system->addItem("DVB-S2");
		ui->comboBox_system->addItem("ATSC");
		ui->comboBox_system->addItem("DVB-C B");
	}

	ui->comboBox_modulation->clear();
	if (dvb->dvb_thread->delsys.contains(SYS_ATSC) || dvb->dvb_thread->delsys.contains(SYS_ATSCMH)) {
		add_comboBox_modulation("VSB 8");
	}
	if (dvb->dvb_thread->delsys.contains(SYS_DVBC_ANNEX_A)) { // DVB-C
		add_comboBox_modulation("QAM 16");
		add_comboBox_modulation("QAM 32");
		add_comboBox_modulation("QAM 64");
		add_comboBox_modulation("QAM 128");
		add_comboBox_modulation("QAM 256");
		add_comboBox_modulation("QAM AUTO");
	}
	if (dvb->dvb_thread->delsys.contains(SYS_DVBC_ANNEX_B)) { // NA Cable
		add_comboBox_modulation("QAM 64");
		add_comboBox_modulation("QAM 256");
	}
	if (dvb->dvb_thread->delsys.contains(SYS_DVBT)) {
		add_comboBox_modulation("QPSK");
		add_comboBox_modulation("QAM 16");
		add_comboBox_modulation("QAM 64");
	}
	if (dvb->dvb_thread->delsys.contains(SYS_DVBT2)) {
		add_comboBox_modulation("QPSK");
		add_comboBox_modulation("QAM 16");
		add_comboBox_modulation("QAM 64");
		add_comboBox_modulation("QAM 256");
	}
	if (dvb->dvb_thread->delsys.contains(SYS_DCII)) {
		add_comboBox_modulation("C QPSK");
		add_comboBox_modulation("I QPSK");
		add_comboBox_modulation("Q QPSK");
		add_comboBox_modulation("C OQPSK");
	}
	if (dvb->dvb_thread->delsys.contains(SYS_DVBS2) || dvb->dvb_thread->delsys.contains(SYS_TURBO)) {
		add_comboBox_modulation("QPSK");
		add_comboBox_modulation("8PSK");
		add_comboBox_modulation("16PSK");
		add_comboBox_modulation("32PSK");
	}
	if (dvb->dvb_thread->delsys.contains(SYS_DVBS)) {
		add_comboBox_modulation("QPSK");
	}

	ui->comboBox_modcod->clear();
	ui->comboBox_modcod->addItem("Auto", 0x0fffffff);
	ui->comboBox_modcod->addItem("QPSK 1/4", 0x01);
	ui->comboBox_modcod->addItem("QPSK 1/3", 0x02);
	ui->comboBox_modcod->addItem("QPSK 2/5", 0x04);
	ui->comboBox_modcod->addItem("QPSK 1/2", 0x08);
	ui->comboBox_modcod->addItem("QPSK 3/5", 0x10);
	ui->comboBox_modcod->addItem("QPSK 2/3", 0x20);
	ui->comboBox_modcod->addItem("QPSK 3/4", 0x40);
	ui->comboBox_modcod->addItem("QPSK 4/5", 0x80);
	ui->comboBox_modcod->addItem("QPSK 5/6", 0x100);
	ui->comboBox_modcod->addItem("QPSK 8/9", 0x200);
	ui->comboBox_modcod->addItem("QPSK 9/10", 0x400);
	ui->comboBox_modcod->addItem("8PSK 3/5", 0x800);
	ui->comboBox_modcod->addItem("8PSK 2/3", 0x1000);
	ui->comboBox_modcod->addItem("8PSK 3/4", 0x2000);
	ui->comboBox_modcod->addItem("8PSK 5/6", 0x4000);
	ui->comboBox_modcod->addItem("8PSK 8/9", 0x8000);
	ui->comboBox_modcod->addItem("8PSK 9/10", 0x10000);
	ui->comboBox_modcod->addItem("16PSK 2/3", 0x20000);
	ui->comboBox_modcod->addItem("16PSK 3/4", 0x40000);
	ui->comboBox_modcod->addItem("16PSK 4/5", 0x80000);
	ui->comboBox_modcod->addItem("16PSK 5/6", 0x100000);
	ui->comboBox_modcod->addItem("16PSK 8/9", 0x200000);
	ui->comboBox_modcod->addItem("16PSK 9/10", 0x400000);
	ui->comboBox_modcod->addItem("32PSK 3/4", 0x800000);
	ui->comboBox_modcod->addItem("32PSK 4/5", 0x1000000);
	ui->comboBox_modcod->addItem("32PSK 5/6", 0x2000000);
	ui->comboBox_modcod->addItem("32PSK 8/9", 0x4000000);
	ui->comboBox_modcod->addItem("32PSK 9/10", 0x8000000);

	if (dvb->dvb_thread->extended_caps & FE_CAN_BLINDSEARCH) {
		ui->gridWidget_delsys->hide();
		ui->gridWidget_blindscan->show();
		ui->gridWidget_satellite->show();
	} else {
		ui->gridWidget_delsys->show();
		if (isVectorQAM(dvb->dvb_thread->delsys) || isVectorATSC(dvb->dvb_thread->delsys) || isVectorDVBT(dvb->dvb_thread->delsys)) {
			ui->gridWidget_blindscan->show();
			ui->gridWidget_satellite->hide();
		} else {
			ui->gridWidget_blindscan->show();
			ui->gridWidget_satellite->show();
		}
	}

	if (dvb->dvb_thread->extended_caps & FE_CAN_MODCOD) {
		ui->gridWidget_modcod->show();
	} else {
		ui->gridWidget_modcod->hide();
	}
}

void spectrumscan_tab::reload_lnb()
{
	mc->reload_settings();

	noload = true;
	ui->comboBox_lnb->clear();
	for (int a = 0; a < MAX_LNBS; a++) {
		if (mc->mysettings->value("lnb"+QString::number(a)+"_enabled").toBool()) {
			ui->comboBox_lnb->insertItem(a, QString::number(a) + " " + mc->mysettings->value("lnb"+QString::number(a)+"_name").toString(), a);
		}
	}
	if (ui->comboBox_lnb->currentIndex() < 0) {
		ui->comboBox_lnb->insertItem(0, "0");
	}
	ui->comboBox_lnb->setCurrentIndex(mc->mysettings->value("adapter"+QString::number(dvb->dvb_thread->adapter)+"_default_lnb").toInt());

	ui->comboBox_frontend->clear();
	for (QString f_num : worker->get_frontends()) {
		ui->comboBox_frontend->insertItem(f_num.toInt(), f_num + " " + mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_frontend" + f_num + "_name").toString(), f_num.toInt());
	}
	ui->comboBox_frontend->setCurrentIndex(0);
	noload = false;

	reload_settings();
	setup_tuning_options();
}

void spectrumscan_tab::reload_settings()
{
	if (noload) {
		return;
	}

	adapter_status();

	int gotox_i = ui->comboBox_gotox->currentIndex();
	ui->comboBox_gotox->clear();
	ui->comboBox_gotox->addItem("0", 0);
	for (int i = 1; i < 256; i++) {
		QString text = mc->mysettings->value("adapter"+QString::number(dvb->dvb_thread->adapter)+"_diseqc_v12_name_"+QString::number(i)).toString();
		if (text != "") {
			ui->comboBox_gotox->addItem(text, i);
		}
	}
	ui->comboBox_gotox->setCurrentIndex(gotox_i);

	noload = true;
	QVariant d(0);
	QVariant e(1|32);
	switch(mc->tune_ops[ui->comboBox_lnb->currentData().toInt()].voltage) {
	case 0: // V
		ui->comboBox_polarity->setItemData(0, e, Qt::UserRole -1);
		ui->comboBox_polarity->setItemData(1, d, Qt::UserRole -1);
		ui->comboBox_polarity->setItemData(2, d, Qt::UserRole -1);
		ui->comboBox_polarity->setCurrentIndex(0); // V
		break;
	case 1: // H
		ui->comboBox_polarity->setItemData(0, d, Qt::UserRole -1);
		ui->comboBox_polarity->setItemData(1, e, Qt::UserRole -1);
		ui->comboBox_polarity->setItemData(2, d, Qt::UserRole -1);
		ui->comboBox_polarity->setCurrentIndex(1); // H
		break;
	case 2: // N
		ui->comboBox_polarity->setItemData(0, d, Qt::UserRole -1);
		ui->comboBox_polarity->setItemData(1, d, Qt::UserRole -1);
		ui->comboBox_polarity->setItemData(2, e, Qt::UserRole -1);
		ui->comboBox_polarity->setCurrentIndex(2); // N
		break;
	case 3: // B
		ui->comboBox_polarity->setItemData(0, e, Qt::UserRole -1);
		ui->comboBox_polarity->setItemData(1, e, Qt::UserRole -1);
		ui->comboBox_polarity->setItemData(2, d, Qt::UserRole -1);
		ui->comboBox_polarity->setCurrentIndex(1); // H
		break;
	}
	noload = false;

	int lnb = ui->comboBox_lnb->currentData().toInt();
	int pol = ui->comboBox_polarity->currentIndex();
	set_chart_details(lnb, pol);
}

void spectrumscan_tab::add_comboBox_modulation(QString name)
{
	if (ui->comboBox_modulation->findText(name) < 0) {
		ui->comboBox_modulation->addItem(name);
	}
}

void spectrumscan_tab::set_colors(int lnb, int pol)
{
	if (dvb->dvb_thread->tune_ops.voltage == 0) { // V
		mc->qwt_lnb.at(lnb)->qwt_pol.at(0)->set_color(dvb->qwt_plot, GREEN);
	}
	if (dvb->dvb_thread->tune_ops.voltage == 1) { // H
		mc->qwt_lnb.at(lnb)->qwt_pol.at(1)->set_color(dvb->qwt_plot, GREEN);
	}
	if (dvb->dvb_thread->tune_ops.voltage == 2) { // N
		mc->qwt_lnb.at(lnb)->qwt_pol.at(2)->set_color(dvb->qwt_plot, GREEN);
	}
	if (dvb->dvb_thread->tune_ops.voltage == 3) { // B
		if (ui->comboBox_polarity->currentIndex() == 0) { // V
			mc->qwt_lnb.at(lnb)->qwt_pol.at(0)->set_color(dvb->qwt_plot, GREEN);
			mc->qwt_lnb.at(lnb)->qwt_pol.at(0)->qwt_curve->setTitle(mc->mysettings->value("lnb" + ui->comboBox_lnb->currentData().toString() + "_name").toString() + " -" + dvbnames.voltage[0]);
			mc->qwt_lnb.at(lnb)->qwt_pol.at(1)->set_color(dvb->qwt_plot, DGREEN);
			mc->qwt_lnb.at(lnb)->qwt_pol.at(1)->qwt_curve->setTitle(mc->mysettings->value("lnb" + ui->comboBox_lnb->currentData().toString() + "_name").toString() + " -" + dvbnames.voltage[1]);
		} else { // H
			mc->qwt_lnb.at(lnb)->qwt_pol.at(0)->set_color(dvb->qwt_plot, DGREEN);
			mc->qwt_lnb.at(lnb)->qwt_pol.at(0)->qwt_curve->setTitle(mc->mysettings->value("lnb" + ui->comboBox_lnb->currentData().toString() + "_name").toString() + " -" + dvbnames.voltage[0]);
			mc->qwt_lnb.at(lnb)->qwt_pol.at(1)->set_color(dvb->qwt_plot, GREEN);
			mc->qwt_lnb.at(lnb)->qwt_pol.at(1)->qwt_curve->setTitle(mc->mysettings->value("lnb" + ui->comboBox_lnb->currentData().toString() + "_name").toString() + " -" + dvbnames.voltage[1]);
		}
	}
	mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_curve->setTitle(mc->mysettings->value("lnb" + ui->comboBox_lnb->currentData().toString() + "_name").toString() + " -" + dvbnames.voltage[pol]);
}

void spectrumscan_tab::	set_chart_details(int lnb, int pol)
{
	mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->f_start = mc->tune_ops[lnb].f_start;
	mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->f_stop  = mc->tune_ops[lnb].f_stop;
	if (!mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->min && !mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->max) {
		mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->min = 0;
		mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->max = 100;
	}
	set_colors(lnb, pol);

	worker->curve_show(lnb, pol);
	ui->qwtPlot->setAxisScale(QwtPlot::xBottom, mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->f_start, mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->f_stop);
	ui->qwtPlot->setAxisScale(QwtPlot::yLeft, mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->min, mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->max);

	ui->qwtPlot->replot();
}

void spectrumscan_tab::adapter_status()
{
	int adapter = dvb->dvb_thread->adapter;
	if (dvb->dvb_thread->status & TUNER_AVAIL) {
		this->setEnabled(true);
		return;
	}
	if (dvb->dvb_thread->status & TUNER_TUNED) {
		this->setEnabled(false);
		dvb->emit_update_status("Adapter: " + QString::number(adapter) + " = Tuned", 5);
		return;
	}
	if (dvb->dvb_thread->status & TUNER_DEMUX) {
		this->setEnabled(false);
		dvb->emit_update_status("Adapter: " + QString::number(adapter) + " = Tuned", 5);
		return;
	}
}

void spectrumscan_tab::qwt_draw(QVector<double> x, QVector<double> y, int min, int max, int lnb, int pol, unsigned int scale)
{
	QString scale_text;
	switch (scale) {
	case SC_DB:
		scale_text = "dB";
		break;
	case SC_DBM:
		scale_text = "dBm";
		break;
	case SC_GAIN:
		scale_text = "AGC Gain";
		break;
	default:
		break;
	}
	ui->qwtPlot->setAxisTitle(QwtPlot::yLeft, scale_text);
	mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_curve->setSamples(x, y);
	mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->min = min;
	mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->max = max;
	worker->marker_remove(lnb, pol);
	set_chart_details(lnb, pol);
}

void spectrumscan_tab::markers_draw(int lnb, int pol)
{
	worker->marker_remove(lnb, pol);

	for (tp_info t : dvb->dvb_thread->tp_try) {
		pol = t.polarity;
		mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_marker.append(new QwtPlotMarker);
		QwtText text;
		if (isSatellite(t.system)) {
			text = QString::number(t.frequency);
		} else {
			text = format_freq(t.frequency, t.system);
		}
		if (t.polarity == ui->comboBox_polarity->currentIndex()) {
			mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_marker.last()->setSymbol(new QwtSymbol(QwtSymbol::Diamond, QBrush(GREEN), QPen(GREEN), QSize(5,5)));
			text.setColor(GREEN);
		} else {
			mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_marker.last()->setSymbol(new QwtSymbol(QwtSymbol::Diamond, QBrush(DGREEN), QPen(DGREEN), QSize(5,5)));
			text.setColor(DGREEN);
		}
		mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_marker.last()->setLabel(text);
		mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_marker.last()->setLabelOrientation(Qt::Vertical);
		mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_marker.last()->setLabelAlignment(Qt::AlignTop);
		mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_marker.last()->setValue(t.frequency, t.spectrumscan_lvl);
		mc->qwt_lnb.at(lnb)->qwt_pol.at(pol)->qwt_marker.last()->attach(ui->qwtPlot);
	}
	ui->qwtPlot->updateLegend();
	ui->qwtPlot->replot();

	worker->myspectrumscan_thread->mutex.unlock();
}

void spectrumscan_tab::qwtPlot_selected(QPointF pos)
{
	if (!dvb->dvb_thread->openfd()) {
		return;
	}
	if (worker->myspectrumscan_thread->isRunning()) {
		dvb->emit_update_status("Please wait for spectrum scan to finish first", 1);
		return;
	}
	if (dvb->dvb_thread->loop) {
		dvb->emit_update_status("Tuner is currently busy", 1);
		return;
	}

	// Make sure we're not allready tuned or blindscanning
	if (!(dvb->dvb_thread->status & TUNER_AVAIL) || dvb->dvb_thread->status & TUNER_SCAN) {
		dvb->emit_update_status("Adapter Busy...", 1);
		return;
	}

	tuning_options tp = dvb->dvb_thread->tune_ops;
	tp.frequency	= (int)pos.x();
	tp.polarity	= ui->comboBox_polarity->currentIndex();

	if (ui->gridWidget_delsys->isVisible()) {
		tp.modulation	= dvbnames.modulation_name.indexOf(ui->comboBox_modulation->currentText());
		tp.system	= dvbnames.system_name.indexOf(ui->comboBox_system->currentText());
		tp.rolloff	= dvbnames.rolloff_name.indexOf("25");
		tp.symbolrate	= ui->comboBox_sr->currentText().toInt();
		tp.fec		= FEC_AUTO;
		tp.modcod	= ui->comboBox_modcod->currentData().toUInt();
	} else {
		tp.modulation	= QPSK;
		tp.system	= SYS_DVBS;
		tp.symbolrate	= ui->comboBox_sr->currentText().toInt();
		tp.fec		= FEC_AUTO;
		tp.modcod	= ui->comboBox_modcod->currentData().toUInt();
	}

	dvb->dvb_thread->tp = tp;
	dvb->dvb_thread->tune_ops = tp;

	emit new_tuning_tab();
}


void spectrumscan_tab::on_checkBox_loop_stateChanged()
{
	worker->myspectrumscan_thread->loop = ui->checkBox_loop->isChecked();
}

void spectrumscan_tab::on_comboBox_modcod_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	if (ui->comboBox_modcod->currentText() != "Auto") {
		ui->gridWidget_delsys->show();
	} else {
		ui->gridWidget_delsys->hide();
	}
}

void spectrumscan_tab::on_pushButton_spectrumscan_clicked()
{
	if (!dvb->dvb_thread->openfd()) {
		return;
	}

	if (!(dvb->dvb_thread->status & TUNER_AVAIL) || dvb->dvb_thread->status & TUNER_SCAN) {
		dvb->emit_update_status("Adapter Busy...", 1);
		return;
	}

	if (ui->checkBox_fast->isChecked()) {
		worker->myspectrumscan_thread->points = 100;
	} else {
		worker->myspectrumscan_thread->points = 500;
	}

	dvb->dvb_thread->tune_ops	= mc->tune_ops.at(ui->comboBox_lnb->currentData().toInt());
	dvb->dvb_thread->tp.system	= dvbnames.system_name.indexOf(ui->comboBox_system->currentText());
	dvb->dvb_thread->tp.polarity	= ui->comboBox_polarity->currentIndex();
	worker->myspectrumscan_thread->loop		= ui->checkBox_loop->isChecked();
	worker->myspectrumscan_thread->loop_delay	= mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_loop_delay").toInt();
	worker->myspectrumscan_thread->lnb		= ui->comboBox_lnb->currentData().toInt();
	worker->myspectrumscan_thread->start();

	worker->curve_hide();
	ui->qwtPlot->replot();
}

void spectrumscan_tab::on_pushButton_blindscan_clicked()
{
	if (!dvb->dvb_thread->openfd()) {
		return;
	}

	// Make sure we're not allready tuned or blindscanning
	if (!(dvb->dvb_thread->status & TUNER_AVAIL) || dvb->dvb_thread->status & TUNER_SCAN) {
		dvb->emit_update_status("Adapter Busy...", 1);
		return;
	}

	dvb->dvb_thread->tp.symbolrate = ui->comboBox_sr->currentText().toInt();;
	if (ui->gridWidget_delsys->isVisible()) {
		dvb->dvb_thread->tp.system	= dvbnames.system_name.indexOf(ui->comboBox_system->currentText());
		dvb->dvb_thread->tp.modulation	= dvbnames.modulation_name.indexOf(ui->comboBox_modulation->currentText());
	} else {
		dvb->dvb_thread->tp.system	= SYS_DVBS;
		dvb->dvb_thread->tp.modulation	= QPSK;
	}

	if (ui->checkBox_smart->isChecked() && dvb->dvb_thread->tp_try.size()) {
		emit new_blinds_tab("smartscan");
	} else {
		if (isSatellite(dvbnames.system_name.indexOf(ui->comboBox_system->currentText()))) {
			emit new_blinds_tab("scan");
		} else {
			emit new_blinds_tab("smartscan");
		}
	}
}

void spectrumscan_tab::on_pushButton_usals_go_clicked()
{
	dvb->dvb_thread->old_position = mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_usals_position").toDouble();
	dvb->dvb_thread->usals_drive(ui->comboBox_usals->currentText().toDouble());
	mc->mysettings->setValue("adapter"+QString::number(dvb->dvb_thread->adapter)+"_usals_position", ui->comboBox_usals->currentText().toDouble());
}

void spectrumscan_tab::on_pushButton_gotox_go_clicked()
{
	int loc   = ui->comboBox_gotox->currentData().toInt();
	float pos = mc->mysettings->value("adapter"+QString::number(dvb->dvb_thread->adapter)+"_diseqc_v12_pos_"+QString::number(loc)).toFloat();
	dvb->dvb_thread->gotox_drive(loc, pos);
}

void spectrumscan_tab::on_pushButton_gotox_save_clicked()
{
	dvb->dvb_thread->gotox_save(ui->comboBox_gotox->currentData().toInt());
}

void spectrumscan_tab::on_pushButton_drive_east_L_clicked()
{
	dvb->dvb_thread->step_motor(0, 5);
}

void spectrumscan_tab::on_pushButton_drive_east_S_clicked()
{
	dvb->dvb_thread->step_motor(0, 1);
}

void spectrumscan_tab::on_pushButton_drive_west_S_clicked()
{
	dvb->dvb_thread->step_motor(1, 1);
}

void spectrumscan_tab::on_pushButton_drive_west_L_clicked()
{
	dvb->dvb_thread->step_motor(1, 5);
}

void spectrumscan_tab::on_comboBox_frontend_currentIndexChanged(int index)
{
	if (index < 0 || noload) {
		return;
	}

	setup_tuning_options();
}

void spectrumscan_tab::on_comboBox_lnb_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	if (noload) {
		return;
	}

	dvb->dvb_thread->tune_ops = mc->tune_ops[ui->comboBox_lnb->currentData().toInt()];
	reload_settings();

	int lnb = ui->comboBox_lnb->currentData().toInt();
	int pol = ui->comboBox_polarity->currentIndex();
	worker->curve_hide();
	worker->curve_show(lnb, pol);
	set_chart_details(lnb, pol);
}

void spectrumscan_tab::on_comboBox_polarity_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	if (noload) {
		return;
	}

	int lnb = ui->comboBox_lnb->currentData().toInt();
	int pol = ui->comboBox_polarity->currentIndex();
	worker->curve_show(lnb, pol);
	set_chart_details(lnb, pol);
}

void spectrumscan_tab::reload_qwtplot()
{
	on_comboBox_lnb_currentIndexChanged(0);
}

void spectrumscan_tab::keyPressEvent(QKeyEvent *event)
{
	if (event->modifiers() == Qt::CTRL && event->key() == Qt::Key_Tab) {
		emit ctrl_tab();
	}
}

void spectrumscan_tab::update_signal()
{
	if (mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_diseqc_v12").toBool() || mc->mysettings->value("adapter" + QString::number(dvb->dvb_thread->adapter) + "_diseqc_v13").toBool()) {
		ui->gridWidget_stats->show();
	} else {
		hide_signal();
	}

	QString lvl = QString::number(dvb->dvb_thread->tp.lvl, 'f', 1);
	if (dvb->dvb_thread->tp.lvl_scale == FE_SCALE_DECIBEL) {
		ui->label_strength->setText(QString("<b><font size=8>%1</font>dBm</b>").arg(lvl));
	} else {
		ui->label_strength->setText(QString("<font size=8><b>%1%</b></font>").arg(lvl));
	}
	QString min = QString::number(dvb->dvb_thread->min_snr(), 'f', 1);
	QString snr = QString::number(dvb->dvb_thread->tp.snr, 'f', 1);
	if (dvb->dvb_thread->tp.snr_scale == FE_SCALE_DECIBEL) {
		ui->label_quality->setText(QString("min:<b><font size=8>%1</font>dB</b><br>cur:<b><font size=8>%2</font>dB</b>").arg(min).arg(snr));
	} else {
		ui->label_quality->setText(QString("<font size=8><b>%1%</b></font>").arg(snr));
	}
}

void spectrumscan_tab::hide_signal()
{
	ui->gridWidget_stats->hide();
}
